/*
 * @Author: Your name
 * @Date:   2020-04-06 23:14:00
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-07 00:21:23
 */
import React from 'react';
import styled,{css} from 'styled-components'

const sizes={
    mobile:'320px',
    tablet:'768px',
    desktop:'1024px'
}

const baseStyles=css`
    
    border: ${props=>`2px solid ${props.borderC}`};
    
`

const Button = styled.button`
    padding:1em 2em;
    margin: 1em;
    ${props=>props.primary && baseStyles}
`


const device={
    mobile:(styles)=>{ 
        return `@media(min-width: ${sizes.mobile}){
            ${styles}
        }`
    },
    tablet:(styles)=>{ 
        return `@media(min-width: ${sizes.tablet}){
            ${styles}
        }`
    },
    desktop:(styles)=>{ 
        return `@media(min-width: ${sizes.desktop}){
            ${styles}
        }`
    }

}

const Header = styled.header`
    background: linear-gradient(35deg, black , yellow  );
    text-align:center;
    border-radius:0.2em;
    color:white;
    padding:0.3em;
    margin:0.3em;
    font-size:24px;

    ${device.mobile`
        background:rgba(255,255,0,0.7);
        font-size:20px;
        color:white;
    `}
    
    ${device.tablet`
    background:rgba(200,100,0,0.7);
    font-size:18px;
    color:white;
    `}
    
    /*${device.desktop`
    background:rgba(0,200,10,0.7);
    font-size:18px;
    color:white;
    `}*/
`
    
const Input=styled.input.attrs((props)=>({
    placeholder:'Ingresa texto',
    type:props.type ||'text'
}))`
    padding:1em;
    border:1px solid blueviolet;

`

const Atributos = () => {
    return ( 
        <div>
        <Header>
        <h1>Styed Components</h1>

        </Header>
            <Input type="date">
            </Input>
        <Button>
            DIsparar
        </Button>
        <Button primary borderC="red">
            boton red
        </Button>
        </div>
     );
}
 
export default Atributos;