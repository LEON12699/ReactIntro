/*
 * @Author: Your name
 * @Date:   2020-03-25 17:34:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-25 22:20:03
 */
import React, { Component } from 'react';
import PropTypes from "prop-types";

class Profile extends Component {
    static propTypes={
        name:PropTypes.string.isRequired,
        bio:PropTypes.string,
        email:PropTypes.string,
        age:PropTypes.number
    }

    static defaultProps={
        name:"Nombre por defecto"
    }

    render() { 
        const {name,bio,email}=this.props
        
        return ( <div>
            <h1>{name}</h1>
            <p>...{bio}</p>
            <a href={`mailto:${email}`}>
                {email}
            </a>
        </div> );
    }
}
 

class Proptipes extends Component {
    state = {  }
    render() { 
        return ( 
        <div>
            <Profile
            
            bio="practicando ando "
            email="israelpat42@gmail.com"></Profile>
        </div> );
    }
}
 
export default Proptipes;