/*
 * @Author: Your name
 * @Date:   2020-03-30 21:39:40
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 22:00:25
 */
import React, { Component } from "react";

const headerStyle = {
    margin: "0.6em",
    borderRadius: "0.3em",
    border: "1px solid #d2d2d2",
    padding: "2em 0.4em",
    fontFamily: "monospace",
    fontSize: "17px",
    textAlign: `center`
};

const BlueStyle = {
    ...headerStyle,
    border: `3px solid blue`
};

const redStyle = {
    ...headerStyle,
    border: `3px solid red`
};

const Header = () => {
    const subtitleStyle = {
        fontWeight: "bold"
    };

    return (
        <header style={headerStyle}>
            <div>(Hermanos )</div>
            <div style={subtitleStyle}>
                Parent Component
        <span role="img" aria-label="flame">
                    🔥
        </span>
            </div>
        </header>
    );
};

class ComponentA extends Component {
  
    render() {
        const { num } = this.props
        return (
            <div style={BlueStyle}>
                <button onClick={this.props.onAdd}>Componet A ({num})</button>
            </div>
        );
    }
}

class ComponentB extends Component {
  
    render() {
        const { num } = this.props;
        return (
            <div style={redStyle}>
                <button onClick={this.props.onAdd}>Componet B ({num})</button>
            </div>
        );
    }
}

class Hermanos extends Component {
    state = {
        countA: 0,
        countB: 0
    };

    handleAddA = () => {
        this.setState(state => (
            {
                countA: state.countA + 1
            }));
    }

    handleAddB = () => {
        this.setState(state => (
            {
                countB: state.countB + 2
            }));
    }
    render() {
        const { countA, countB } = this.state
        return (
            <div>
                <Header />
                <ComponentA
                    num={ countA }
                    onAdd={this.handleAddB} />
                <ComponentB
                    num={ countB }
                    onAdd={this.handleAddA} />
            </div>
        );
    }
}

export default Hermanos;
