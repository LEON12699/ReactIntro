/*
 * @Author: Your name
 * @Date:   2020-03-31 00:17:57
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 00:31:59
 */
import React, { Component } from 'react';
import "./styles.css";
import Header from "../header";

const myHOC =(Comp)=>{
    return(config={click:0,sumClicks:1})=> class  extends Component {
        state={
            num:config.click
        }

        add=()=>{
            this.setState(state=>({ num: state.num+config.sumClicks }));
        }
        render() { 
            return ( 
                <Comp
                num={this.state.num}
                add={this.add}></Comp>
             );
        }
    }
     

}
class HOC extends Component {
    state = {  }
    render() { 
        const {num,add}=this.props
        return ( 
            <div className="headerStyle">
                <Header title="HIJO <-> PADRE"
                    subtitle="HOC High order Component 🔥"
                ></Header>
                <h1>{num}</h1>
                <button onClick={add}>
                    ADD
                </button>
            </div>
         );
    }
}
 
export default myHOC(HOC)({
    click:10,
    sumClicks:5
});