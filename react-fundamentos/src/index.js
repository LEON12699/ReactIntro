/*
 * @Author: Your name
 * @Date:   2020-03-28 21:38:05
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-02 22:45:21
 */
import React from 'react'
import ReactDOM from 'react-dom'
import PropChildren from './Utilidades/propChildren'
import ComposicionImplicita from './Utilidades/ComposicionImplicita'
import LazySuspense from './LazySuspense'
/*import EhookHttp2 from './componentes/ComponentesFuncionales./Ejemplos /EhookHttp2'
import App3 from './componentes/Apps'
import MConstructo from './cicloDeVida/MConstructor'
import MConstructor from './cicloDeVida/MConstructor'
import MComponentdidmount from './cicloDeVida/MComponentdidmount'
import MComponentdidupdate from './cicloDeVida/MComponentdidupdate'
import MGetsnapshotbeforeupdate from './cicloDeVida/MGetsnapshotbeforeupdate'
import MgetDeliveryStateFromProps from './cicloDeVida/MgetDeliveryStateFromProps'
import MshouldComponentUpdate from './cicloDeVida/MshouldComponentUpdate'
import MWillUnmount from './cicloDeVida/MWillUnmount'
import Instancia from './comunicacion/Instancias'
import EventBuilder from './comunicacion/EventBuilder'
import Hermanos from './comunicacion/Hermanos'
import ObserverPattern from './comunicacion/ObserverPattern'
import VariablesG from './comunicacion/VariablesG'
import ApiContext from './comunicacion/ApiContext'
import RenderProps from './comunicacion/RenderProp'
import RenderProps2 from './comunicacion/RenderProps2'
import HOC from './comunicacion/HOC'
import HOC2 from './comunicacion/HOC2'
import UseState from './componentes/ComponentesFuncionales./UseState'
import UseState2 from './componentes/ComponentesFuncionales./UseState2'
import UseStateObjects from './componentes/ComponentesFuncionales./UseStateObjects'
import HookStateMultiple from './componentes/ComponentesFuncionales./HookStateMultiple'
import HookUseEffect from './componentes/ComponentesFuncionales./HookUseEffect'
import HookUseEffect2 from './componentes/ComponentesFuncionales./HookUseEffect2'
import HookUseEffectHttp from './componentes/ComponentesFuncionales./UseEffectHttp'
import HookuseLayoutEffect from './componentes/ComponentesFuncionales./HookuseLayoutEffect'
import UseContext from './componentes/ComponentesFuncionales./UseContext'
import UseRef from './componentes/ComponentesFuncionales./UseRef'
import Ejemplo1 from './componentes/ComponentesFuncionales./Ejemplos /Ejemplo1'
import Ejemplo2Terceros from './componentes/ComponentesFuncionales./Ejemplos /Ejemplo2Terceros'
import UseReducer from './componentes/ComponentesFuncionales./UseReducer'
import UseImperativeHandle from './componentes/ComponentesFuncionales./UseImperativeHandle'
import Memo from './componentes/ComponentesFuncionales./Memo'
import MemoCallback from './componentes/MemoCallback'
import UseMemo2 from './componentes/ComponentesFuncionales./UseMemo2'
import UseDebugValue from './componentes/ComponentesFuncionales./UseDebugValue'

import EhookHttp from './componentes/ComponentesFuncionales./Ejemplos /EhookHttp'
*/
/*const name ="Constante {}"

const user={
  name:"Israel leon",
  edad:20,
  ciudad:"Loja"
}


function GetInfo(user1){
  return `mi nombre es ${user1.name} y soy de ${user1.ciudad}`
}


}

/*const App =<div><h1>React {name} </h1>
            <div>
              <p>MI nombre es {user.name},</p>
              <p>MI edad es {user.edad} el dobles es {user.edad *2}</p>
            </div>
            </div>
*/

class Componente extends React.Component{
/*
  constructor(){
    super()
    
    
    const METHODS = [
    `agregar`,
    `restar`,
    `limpiar`
    ]

  METHODS.forEach((method) => {
    console.log(this)
    this[method] = this[method].bind(this)
  })

    this.state={
      cantidad:0
    }
  }*/

  state={
    cantidad:0
  }
 
  // arrow()=>{} functions heredan el this del padre mas no las funciones

  limpiar=()=>this.setState({cantidad:0})
  


  agregar=()=>{
    this.setState({cantidad:this.state.cantidad+ 1})
  }

  restar=()=>{
    this.setState({cantidad:this.state.cantidad- 1})
  }

  render(){
    return(
      <div>
        <h1>{this.props.name} ${this.props.price}</h1>
        <div>Cantidad: {this.state.cantidad}</div>
        <button 
        onClick={this.restar}>
        -
        </button>
        <button 
        onClick={this.agregar}>
        +
        </button>
        <button 
        onClick={this.limpiar}>
        Limpiar
        </button>
        
        <p>Mi primer componente</p>
        <hr/>
      </div>
    )
  }
}

/* funcion componente
const Componente=(props)=>{
  return(
    <div>
      <h1>{props.name} ${props.price}</h1>
      <p>Mi primer componente</p>
    </div>
  )
  }
//const App2 =<h1>{ GetInfo(user) }</h1>

const App2 = () => (
  <div>
    <Componente name="Israel" price={2.00}>    </Componente>
    <Componente name="Vacio" price={5.00}/>
    <Componente/>
  </div>
)*/
//const root= document.getElementById('root')
//ReactDOM.render(App2,document.getElementById('root'))

//ReactDOM.render(<App3/>,document.getElementById('root'))
//ReactDOM.render(<MConstructor/>,document.getElementById('root'))
//ReactDOM.render(<MComponentdidmount/>,document.getElementById('root'))
//ReactDOM.render(<MComponentdidupdate/>,document.getElementById('root'))
//ReactDOM.render(<MGetsnapshotbeforeupdate/>,document.getElementById('root'))
//ReactDOM.render(<MgetDeliveryStateFromProps/>,document.getElementById('root'))
//ReactDOM.render(<MshouldComponentUpdate/>,document.getElementById('root'))
//ReactDOM.render(<MWillUnmount/>,document.getElementById('root'))
//ReactDOM.render(<Instancia/>,document.getElementById('root'))
//ReactDOM.render(<EventBuilder/>,document.getElementById('root'))
//ReactDOM.render(<Hermanos/>,document.getElementById('root'))
//ReactDOM.render(<ObserverPattern/>,document.getElementById('root'))
//ReactDOM.render(<VariablesG/>,document.getElementById('root'))
//ReactDOM.render(<ApiContext/>,document.getElementById('root'))
//ReactDOM.render(<RenderProps2/>,document.getElementById('root'))
//ReactDOM.render(<HOC2/>,document.getElementById('root'))
//ReactDOM.render(<UseState2/>,document.getElementById('root'))
//ReactDOM.render(<UseStateObjects/>,document.getElementById('root'))
//ReactDOM.render(<HookStateMultiple/>,document.getElementById('root'))
//ReactDOM.render(<HookUseEffect2/>,document.getElementById('root'))
//ReactDOM.render(<HookUseEffectHttp/>,document.getElementById('root'))
//ReactDOM.render(<UseContext/>,document.getElementById('root'))
//ReactDOM.render(<UseRef/>,document.getElementById('root'))
//ReactDOM.render(<Ejemplo1/>,document.getElementById('root'))
//ReactDOM.render(<Ejemplo2Terceros/>,document.getElementById('root'))
//ReactDOM.render(<UseReducer/>,document.getElementById('root'))
//ReactDOM.render(<UseImperativeHandle/>,document.getElementById('root'))
//ReactDOM.render(<Memo/>,document.getElementById('root'))
//ReactDOM.render(<MemoCallback/>,document.getElementById('root'))
//ReactDOM.render(<UseMemo2/>,document.getElementById('root'))
//ReactDOM.render(<UseDebugValue/>,document.getElementById('root'))
//ReactDOM.render(<EhookHttp2/>,document.getElementById('root'))
//ReactDOM.render(<PropChildren/>,document.getElementById('root'))
//ReactDOM.render(<ComposicionImplicita/>,document.getElementById('root'))
ReactDOM.render(<LazySuspense/>,document.getElementById('root'))


//ReactDOM.render(App,root)