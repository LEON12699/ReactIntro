/*
 * @Author: Your name
 * @Date:   2020-03-27 00:24:32
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 00:36:10
 */
import React, { Component } from 'react';

const Emoji=()=>(
    <span
    role="img"
    aria-label="emoji">
    🌱
    </span>
)

class InputNC extends Component{

    nombre= React.createRef()
    email=React.createRef()
    handleClick=()=>{
        const nombre = this.nombre.current.value
        const email = this.email.current.value
        
        //Manejar datos
        this.props.onSend({nombre,email})
    }

    render(){
        return(
            <div>
                <input type="text" ref={this.nombre}
                placeholder="nombre"></input>
                <input type="email"
                ref={this.email}
                placeholder="email"></input>
                <button onClick={this.handleClick} >Enviar</button>
            </div>
        )
    }
}

class InputsNoControl extends Component {
    send =(data)=>{
        console.log(data)
    }

    render() { 
        return ( 
            <div>
                <h1>Input no controlados Refs <Emoji></Emoji></h1>
                <InputNC onSend={this.send}/>   
            </div>
         );
    }
}
 
export default InputsNoControl;