/*
 * @Author: Your name
 * @Date:   2020-03-28 22:03:50
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 22:24:25
 */
import React, { Component } from 'react';


class Events extends Component {
    state = { 
        width:window.innerWidth
     }
    componentDidMount(){
        window.addEventListener('resize',this.handleResize)
        
    }

    handleResize=()=>{
        this.setState({ 
            width: window.innerWidth });
    }

    render() { 
        return (
            <div>
                <h2>Events {this.state.width}</h2>
            </div>
          );
    }
}
 


class Http extends Component {
    state = { 
        photos:[]
     }

    //solo se ejecuta una vez perfectos para peticiones
    // AGREGAR EVENTLISTERNERS  
    componentDidMount(){
        fetch('https://jsonplaceholder.typicode.com/photos')
        .then(res=>res.json())
        .then(photos=>this.setState({photos}))
    }

    render() { 
        const {photos}= this.state
        return ( 
            <div>
                {photos.map(photo=>
                <img
                key={photo.id}
                src={photo.thumbnailUrl}
                alt={photo.title}>

                </img>)}
            </div>
         );
    }
}

class MComponentdidmount extends Component {
    

    render() { 
        //const {photos}= this.state
        return ( 
            <div>
                <h1>Component DidMount</h1>
                <Http/>
                <Events/> 
            </div>
         );
    }
}
 
export default MComponentdidmount;