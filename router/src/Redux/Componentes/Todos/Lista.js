/*
 * @Author: Your name
 * @Date:   2020-04-09 00:35:35
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:43:49
 */
import React from 'react';
import Ptodo from './ptodo';

const Lista = ({todo,updateTodo,deleteTodo}) => {
    return ( 
        <ul>
                {todo.todos.map(todo=>(
                    <Ptodo 
                    key={todo.id}
                    todo={todo}
                    updateTodo={updateTodo}
                    deleteTodo={deleteTodo}/>
                ))}
            </ul>
     );
}
 
export default Lista;