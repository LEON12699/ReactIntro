/*
 * @Author: Your name
 * @Date:   2020-04-01 16:54:40
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-01 22:57:23
 */
import React, { useState, useEffect } from 'react';
import Header from '../Header_cf';

const useFetch = (url,initialState=[]) => {
    const [data, setData] = useState(initialState);
    const [isFetching, setIsFetching] = useState(true);
    useEffect(() => {
        setIsFetching(true)
        fetch(url)
            .then(res => res.json())
            .then(data => {
                setData(data)
                setIsFetching(false)
            })

    }, [url]);

    return [
        data,
        isFetching
    ]
}
const EhookHttp = () => {
    const [imagenes, carga] = useFetch('https://jsonplaceholder.typicode.com/photos/')
    const [clicks, setClicks] = useState(1);
    const [image, cargauno] = useFetch("https://jsonplaceholder.typicode.com/photos/"+clicks)
    
    const add =()=>{
        setClicks(clicks+1)
    }

    return (
        <div>
            <Header
                title="hook personalizado"
                subtitle="solicitud http"
            />
            {carga && <h1>Cargando..-</h1>}
            <button onClick={add}>
                Aumentar ({clicks})
            </button>
            {cargauno && <h1>Cargando.....</h1>}
            <div style={{textAlign:'center',margin:'4em'}}>
                            <h3>{image.title}</h3>
                            <img
                                src={image.url}
                                alt={image.thumbnailUrl}
                                width="100"
                                height="100">
                            </img>
            </div>
            <ol>
                {imagenes.map((imagen,index) => (
                    <li key={imagen.id}>
                        <div style={{textAlign:'center',margin:'2em',background:'#dedede'}}>
                            <h3>{imagen.title}</h3>
                            <img
                                src={imagen.url}
                                alt={imagen.thumbnailUrl}
                                width="100"
                                height="100">
                            </img>
                        </div>
                    </li>
                ))
                }
            </ol>
        </div>
    );
}

export default EhookHttp;