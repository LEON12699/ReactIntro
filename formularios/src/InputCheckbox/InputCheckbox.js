/*
 * @Author: Your name
 * @Date:   2020-03-27 21:07:44
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 21:16:12
 */
import React, { Component } from 'react';

class INputCheckbox extends Component {
    state = { 
        active:true
     }

     handleChange=(event)=>{
        this.setState({ active: event.target.checked });
     }
    render() { 
        const {active} =this.state
        return ( 
            <div>
            {active && (
                <h1>
                    Etiqueta Checkbox 📰
                </h1>
                )}
                <form>
                    <input 
                    onChange={this.handleChange}
                    type="checkbox" checked={this.state.active}></input>
                </form>
            </div>
         );
    }
}
 
export default INputCheckbox;