/*
 * @Author: Your name
 * @Date:   2020-04-09 23:29:20
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:51:04
 */
export const suma = (a,b) => {
    return ( a+b );
}
 
export const resta = (a,b) => {
    return ( a-b );
}

export const divison = (a,b) => {
    return ( a/b );
}

export const multiplicacion = (a,b) => {
    return ( a*b );
}

export const getRandomNum = (min,max) => {
    return ( Math.floor(Math.random()*(max-min+1)+min) );
}