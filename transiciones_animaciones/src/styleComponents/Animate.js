/*
 * @Author: Your name
 * @Date:   2020-04-07 00:23:06
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-07 01:17:04
 */
import React from 'react';
import styled,{ keyframes } from 'styled-components'

const anima= keyframes`
    from{
        background:#000;
    }
    to{
        background:PaleTurquoise;
    }
`

const anima2= keyframes`
    0%{
        background:#000;
        transform:scale(1);
    }

    50%{
        background:PaleTurquoise;
        transform:scale(0.7);
    }

    100%{
        background:#000;
        transform:scale(1);
        
    }
`

const pulse =keyframes`
    0%{
        transform:scale(1);
        background:gray;
        color:black;
    }
    50%{
        transform:scale(1.3);
        background:purple;
        color:white;
    }
    100%{
        transform:scale(1);
        background:gray;
        color:black;
    }
`

const Button = styled.button`
    padding: 1em 2.5em;
    margin: 1em;

    &:hover{
        animation:${pulse} 2s ease-in-out;
    }
`
const Header = styled.header`
    background:linear-gradient(180deg,black,#00008a);
    text-align:center;
    border-radius:0.2em;
    color:white;
    padding:0.3em;
    margin:0.3em;
    font-size:14px;
    /*animation:${anima2} 2s ease-in-out 6;*/
    `

const Annimate = () => {
    return ( 
        <div>
            <Header>
                <h1>Animaciones con styled components</h1>
            </Header>
            <Button>
                boton
            </Button>
        </div>
     );
}
 
export default Annimate;