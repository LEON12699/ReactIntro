/*
 * @Author: Your name
 * @Date:   2020-04-09 01:12:36
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 01:18:27
 */
import React from 'react';
import { connect } from 'react-redux'

const Controller = (props) => {
    console.log(props)
    
    const handler=()=>{
        props.dispatch((dispatch)=>{
            setTimeout(()=>{
                dispatch({
                    type:'INCREMENT'
                })
                dispatch({
                    type:'INCREMENT'
                })
                dispatch({
                    type:'INCREMENT'
                })
            },2000)
        })
    }

    return ( 
        <div>
            <button onClick={handler}>
                Dispatch
                <h1>{props.state.counter}</h1>
            </button>
        </div>
     );
}

const mapStateToProps =(state)=>{
    return{
        state
        
    }
}
 
export default connect(mapStateToProps)(Controller);