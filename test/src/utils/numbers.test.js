/*
 * @Author: Your name
 * @Date:   2020-04-09 23:31:52
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 23:46:35
 */
import {getRandomNum,suma,resta,multiplicacion,divison} from './numbers'

describe('Pruebas funciones matematicas', () => {
    /**to be compara la igualda */
    test('Probando la funcion suma', () => {
        expect(suma(5,5)).toBe(10)
    });
    
    test('Probando la funcion resta', () => {
        expect(resta(5,5)).toBe(0)
    });

    test('Probando la funcion multiplicacion', () => {
        expect(multiplicacion(5,5)).toBe(25)
    });

    test('Probando la funcion divison', () => {
        expect(divison(5,5)).toBe(1)
    });

    test('Probando la funcion random', () => {
        expect(getRandomNum(5,10)).toBeGreaterThanOrEqual(5)
        expect(getRandomNum(5,10)).toBeLessThanOrEqual(10)
 
    });
});