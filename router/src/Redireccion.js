/*
 * @Author: Your name
 * @Date:   2020-04-07 17:30:37
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 00:12:31
 */
import React from 'react';
import {BrowserRouter,Route,Redirect,NavLink} from 'react-router-dom'

//link sin que se recargue la pagina
const isAuth= false


const Perfil = () => {
    return ( 
        isAuth?<h2>Bienvenido al perfil</h2>:<Redirect to={{
            pathname:'/login',
            state:{
                message:"Debes registrate"
            }
        }}/>
     );
}

const Home = (Redireccion) => {
    console.log(Redireccion)
    return ( 
        <h2>Home</h2>
     );
}
 
const Login = ({location}) => {
    
    return ( 
    <>
    {location.state?<h2>{location.state.message}</h2>:
    <h1>Logeate</h1>}
    </>
     );
}

const navStyles= {
    display:'flex',
    justifyContent:'space-around',
    lineHeight:3
}

const activeStyles= {
    color:'purple'
}

/** replace =>remplaza el historial con la ruta anterior*/
const Navigation = () => {
    return ( 
        <nav style={navStyles}>
            <NavLink exact to='/' activeStyle={activeStyles} >home </NavLink>
            <NavLink to="/login" activeStyle={activeStyles}>login </NavLink>
            <NavLink to="/Perfil" replace activeStyle={activeStyles} >Perfil </NavLink>
        </nav>
     );
}




    // exact para que solo cunado sea /login y no tambine /login/ex..
 // strict se puede añadir para que sea estricto
// sensitive sensible a mayusculas y minisculas
// Componente se puede pasar un arrow function con marcado 
// cada vez de nuevo renderizados se regeneran 
// con render no se crea nuevas funciones

const NavigationImperative=({match,location,history})=>(
    <div>
        <button onClick={history.goBack}>
            Atraz
        </button>
        <button onClick={history.goForward}>
        adelante
        </button>
        <button onClick={
            ()=>{
                history.go(1)
            }
        }>GO 2</button>
        <button onClick={()=>{
            history.push('/ninja')
        }}>go ninja
        </button>
        <button onClick={()=>{
            history.replace('/ninja')
        }}>replace
        </button>
    </div>
)

const Ninja = () => {
    return ( 
        <h1>Ninja</h1>
     );
}
 
const Redireccion = () => {
    return ( 
        <BrowserRouter>
        <Navigation/>
            <Route render={NavigationImperative}></Route>
            <Route path='/' exact component={Home}/>
            <Route path='/login'  render={Login}/>
            <Route path='/perfil'  component={Perfil}/>
            <Route path='/ninja' component={Ninja}></Route>
            <Redirect from='p' to='/perfil'/>
            
        </BrowserRouter>
     );
}
 
export default Redireccion;