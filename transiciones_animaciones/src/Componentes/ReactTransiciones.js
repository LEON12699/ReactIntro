/*
 * @Author: Your name
 * @Date:   2020-04-05 16:25:42
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-05 16:49:46
 */
import React,{useState} from 'react';
import {TransitionGroup,CSSTransition} from 'react-transition-group';
import './transitions.css'

const ReactTransiciones = () => {
    const [clicks, setClicks] = useState(0);

    const add=()=>setClicks(clicks+1)
    const subtract =()=>setClicks(clicks-1)


    return ( 
        <div>
            <button onClick={add}>
                +
            </button>
            <button onClick={subtract}>
                -
            </button>
            <div className="box">

            
            <TransitionGroup>
            <CSSTransition
                timeout={1000}
                classNames='fade'
                key={clicks}>
            <div>
                {clicks}
            </div>
            </CSSTransition>
            </TransitionGroup>
            </div>
        </div>
     );
}
 
export default ReactTransiciones;