/*
 * @Author: Your name
 * @Date:   2020-03-25 16:49:32
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-25 17:07:58
 */
import React, { Component} from 'react';
import ReactDOM from "react-dom";

class PortalModal extends Component {

    
        render({children,saluda}=this.props) { 
            if(!saluda){
                return null;
            }
            
        const styles={
            width:`100%`,
            height:`100%`,
            position:`absolute`,
            top:`0`,
            left:`0`,
            background:`linear-gradient(to top right, #667eea, #764ba2)`,
            opacity:`0.95`,
            color:`#FFF`

        }
        return ReactDOM.createPortal((
            <div style={styles}>
                {}<h1>{children}  </h1>
            </div>
        ),document.getElementById('modal-root'));
    }
}


export default PortalModal;