/*
 * @Author: Your name
 * @Date:   2020-04-09 01:28:02
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 01:58:48
 */
import React from 'react';
import {connect} from 'react-redux'
import {fetchPosts} from '../actions/BlogActions'
import {SyncLoader} from 'react-spinners'

const Blog = (props) => {
    console.log(props)
    const click=()=>{
        props.dispatch(fetchPosts())
    }


    return ( 
        <div>
        <h1>Nuevas entradas blogs</h1>
        <button onClick={click}>
            FETCH posts
        </button>
        {props.posts.isFetching?
        (<SyncLoader/>):
        (<ul >
            {props.posts.posts.map(post=>(
                <li key={post.id} style={{margin:'10px',border:'1px solid black'}}>
                    {post.title}
                    <ul>
                    <li style={{fontStyle:'italic'}}>{post.body}</li>
                    </ul>
                </li>
            ))}
        </ul>)}
        
        </div>
    );
}

export default connect((state)=>{
    return state
})(Blog);