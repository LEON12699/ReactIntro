/*
 * @Author: Your name
 * @Date:   2020-03-28 23:04:43
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 23:50:29
 */
import React, { Component } from 'react';
import Faker from "faker/locale/es"

const chatStyle={
    width:230,
    height:300,
    border:`1px solid gray`,
    overflow:`auto`,
    fontFamily:`monospace`
}

const messageStyle={
    padding:`1em`,
    borderBottom:`1px solid #DDD`
}

const avatarStyle={
    width:50,
    height:50,
    borderRadius:50
}
class Chat extends Component {
    box=React.createRef()

    getSnapshotBeforeUpdate(){
        const box=this.box.current
    
        if(box.scrollTop+box.offsetHeight>= box.scrollHeight){ 
            return true
        }
        return false
    }
    componentDidUpdate(prevProp,prevState,snapshot){
       const box=this.box.current
       if(snapshot){
           box.scrollTop=box.scrollHeight
       }
    }

    render() { 
        return ( 
        <div 
        ref={this.box}
        style={chatStyle}>
            {this.props.list.map(item=>(
                <div key={item.id} style={messageStyle} >
                    <p>{item.message}</p>
                    <div 
                    >
                        <img
                        src={item.avatar}
                        alt="Avatar"
                        style={avatarStyle}>
                            
                        </img>
                    </div>
                </div>
            ))}
        </div> );
    }
}
 

class MGetsnapshotbeforeupdate extends Component {
    state = { 
        list:[]
     }
     title=React.createRef()


     dispatch=()=>{
        const message={
            id:Faker.random.uuid(),
            name:Faker.name.findName(),
            avatar:Faker.image.avatar(),
            message:Faker.hacker.phrase()
        }
        //console.log(message)
        this.setState(state=>({
            list:[
                ...state.list,
                message
            ]
        }),()=>{
            console.log(message)
        });
    }

   

    render() { 
        return (  
            <div>
                <h1>MGetsnapshotbeforeupdate</h1>
                <Chat list={this.state.list}/>
                <button onClick={this.dispatch}>
                    new message
                </button>
            </div>
        );
    }
}
 
export default MGetsnapshotbeforeupdate;