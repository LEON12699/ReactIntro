/*
 * @Author: Your name
 * @Date:   2020-04-10 02:05:15
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:05:15
 */
import React from 'react';

const List = ({title,lista}) => {
    return (
        <section>
            <h1>{title}</h1>
            <ul>
                {lista.map(item=>(
                    <li key={item.id}>
                        {item.name}
                    </li>
                ))}
                
            </ul>
        </section>
      );
}
 
export default List;