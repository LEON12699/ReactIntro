/*
 * @Author: Your name
 * @Date:   2020-04-07 22:56:59
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-07 23:33:35
 */
import React from 'react';
import {BrowserRouter,Route,Link,NavLink} from 'react-router-dom'
import queryString from 'query-string'

const Hola = () => {
    return ( 
        <h1>HOLA</h1>
     );
}

const Product = () => {
    return ( 
    <div>
        <h2>Productos</h2>
        <Link to='/products/gamers'> Gamers</Link>
        <Link to='/products/hogar'> Hogar</Link>
    </div>
);
}

const Home = (props) => {
    console.log(props)
    return ( 
        <h2>Home</h2>
     );
}

const Navigation = () => {
    return ( 
        <nav style={navStyles}>
            <Link to={{
                pathname:'/',
                search:'?ordenar=nombre',
                hash:'#hash',
                state:{
                    name:'Ninja PRO',
                    age:25
                }}} >home </Link>
            <Link to="/hola">HOla </Link>
            <Link to="/products" replace >Productos </Link>
            <Link to="/ropa" >ROpa</Link>
        </nav>
     );
}

const navStyles= {
    display:'flex',
    justifyContent:'space-around'
}

 
const ProductCategory = ({match}) => {
    console.log(match)
    return ( 
        <div>
            <h1>categoria {match.params.categoria}</h1>
        </div>
     );
}

const Ropa = (props) => {
    const query= queryString.parse(props.location.search)//new URLSearchParams(props.location.search)
    const {color,talla}=query//query.get('color')
    //const {talla} =//query.get('talla')
    return ( 
        <div>
            <h1>Ropa </h1>
            <div>
                COLOR:  {color}<br></br>
                TALLA:  {talla}
            </div>
        </div>
     );
}
 
// el ? para que el params sea opcional
const Rutas = () => {
    return ( 
        <BrowserRouter>
            <Navigation/>
            <Route render={Home}  exact path='/'></Route>
            <Route render={Hola} path='/hola'></Route>
            <Route render={Product} exact path='/products'></Route>
            <Route render={ProductCategory} path='/products/:categoria'></Route>
            <Route render={Ropa} path='/ropa'></Route>
        </BrowserRouter>
     );
}
 
export default Rutas;