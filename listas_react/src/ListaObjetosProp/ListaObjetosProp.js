/*
 * @Author: Your name
 * @Date:   2020-03-25 23:36:34
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 17:42:04
 */
import React, { Component } from 'react';


class ListaObjetosProp extends Component {
    state = { 

        user:{
            name:`Israel Leon`,
            country:`Ecuador`,
            twitter:`twitter`,
            email:`israelpat42@gmail.com`
        }
    }
    render() { 
        const { user }=this.state
        const keys =Object.keys(user)//->['name','country','twiiter','email']
        return ( 
            <div>
                <h2>
                    Iterando propiedades de Objetos 🌠
                </h2>
                <ul>
                {keys.map(key=>(
                        <li>
                            <strong>{key} : </strong>{user[key]}
                        </li>
                    ))}
                </ul>
            </div>
         );
    }
}
 
export default ListaObjetosProp;