/*
 * @Author: Your name
 * @Date:   2020-03-24 16:12:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 16:21:06
 */
import React, { Component } from 'react';
import logo from './logo.svg';
import './App.css';

class Contador extends Component{

  state={
    video:{
      title: 'SUper video',
      likes:0
    }  
  }

  add =()=> {
    this.setState((prevStatus)=>({
      video:{
        ...prevStatus.video, // se clona el objeto con el spread y permite se remplacen con lo ultimo que se a{ade}
        likes:prevStatus.video.likes++
      }
    }))
  }

  render(){
    return(
      <div>
      <h1>{this.state.video.title}</h1>
      <button onClick={this.add}>Likes: ({this.state.video.likes}) </button>
      </div>
    )
  }
}

function App() {
  return (

    
    <div className="App">
    <Contador></Contador>
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.js</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
