/*
 * @Author: Your name
 * @Date:   2020-04-01 16:54:40
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-02 00:22:29
 */
import React, { useState, useEffect } from 'react';
import Header from '../Header_cf';

const useFetch = (url,initialState=[]) => {
    
    const [data, setData] = useState(initialState);
    const [isFetching, setIsFetching] = useState(true);
    useEffect(() => {
        setIsFetching(true)
        fetch(url)
            .then(res => res.json())
            .then(data => {
                setData(data)
                setIsFetching(false)
            })

    }, [url]);

    return [
        data,
        isFetching
    ]
}


const EhookHttp2 = () => {
    const [users, carga] = useFetch('https://jsonplaceholder.typicode.com/users/')
    const [clicks, setClicks] = useState(0);

    
    const add =()=>{
        setClicks(clicks+1)
    }

    return (
        <div>
            <Header
                title="hook personalizado"
                subtitle="solicitud http"
            />
            {carga && <h1>Cargando..-</h1>}
            <button onClick={add}>
                Aumentar ({clicks})
            </button>
           
            <ol>
                {users.map((user,index) => (
                    <li key={user.id}>
                        <div style={{textAlign:'center',margin:'2em',background:'#dedede'}}>
                            <h3>{user.name}</h3>
                            <p> {user.username} <a href="/">{user.email}</a></p>
                        </div>
                    </li>
                ))
                }
            </ol>
        </div>
    );
}

export default EhookHttp2;