/*
 * @Author: Your name
 * @Date:   2020-03-25 22:57:19
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 17:41:31
 */

import React, { Component } from 'react';
import FakerData from '../user'
const frutas=[
    `fresa`,
    `manzana`,
    `sandia`,
    `kiwi`,
    `durazno`,
    `mango`,
    `piña`
]
/*
const FakerD = new FakerData()
const frutas = FakerD.crear_usuarios(10)
*/

class Lista1 extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <ul>
                    {frutas.map((fruta)=>{
                        return(
                            <li>{fruta}</li>
                        )
                    })}
                </ul>
            </div>
         );
    }
}
 
export default Lista1;