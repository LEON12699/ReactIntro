/*
 * @Author: Your name
 * @Date:   2020-03-25 23:04:20
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-25 23:18:02
 */
import React, { Component } from 'react';

class ListaObjetos extends Component {
    state = { 
        products:[
            {
                id:1,
                name:`camisa unicornio`,
                colors:[`#467367`,`#05fd88`,`#a706ee`],
                price:20
            },
            {
                id:2,
                name:`jeans`,
                colors:[`#dedede`,`#123666`,`#a0123d`],
                price:32

            },
            {
                id:3,
                name:`dress`,
                colors:[`#898989`,`#000`,`#FFF`],
                price:100

            },
            {
                id:4,
                name:`cap`,
                colors:[`#dedede`,`#123666`,`#a0123d`],
                price:12

            }
        ]
     }
    render() { 
        return ( 
        <div>
            <h1>Iterando objetos 🌟</h1>
            <div>
                {this.state.products.map(
                    (pro)=>{
                        return(
                            <div>
                               $ {pro.price} - {pro.name}
                               <div>
                                   {pro.colors.map((color)=>{
                                       return(
                                           <span
                                           style={{
                                               width:`23px`,
                                               height:`23px`,
                                               borderRadius:`0.1em`,
                                               border:`1px solid gray`,
                                               display:`inline-block`,
                                               margin:`0.1em`,
                                               background:color
                                           }}>

                                           </span>
                                       )
                                   })}
                               </div>
                            </div>
                        )
                    }
                )}
            </div>

        </div> );
    }
}
 
export default ListaObjetos;