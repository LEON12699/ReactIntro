/*
 * @Author: Your name
 * @Date:   2020-04-03 00:21:51
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-03 21:50:22
 */
import React, { useState } from 'react';
import Header from './Header';

const TransicionesLinea = () => {

    const [active, setActive] = useState(false);

    const toogle = () => setActive(!active)


    return (
        <div>
            <button
                onClick={toogle}>
                    {active?'Desactivar':'Activar'}
            </button>
            <Header 
            title="Transiciones Css en linea"
            show={active} />
        </div>
    );
}

export default TransicionesLinea;