/*
 * @Author: Your name
 * @Date:   2020-04-08 22:13:54
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:36:48
 */
import React from 'react';
import { connect} from 'react-redux'
import {} from '../../Store'
import { addTodo,updateTodo,delteTodo } from '../../actions/TodoActions'
import Formulario from './formularios';
import Lista from './Lista';

const Todos = ({todo,addTodo,updateTodo,deleteTodo}) => {
    console.log(todo)
     const handleSubmit=(e)=>{
         e.preventDefault()
         const text = e.target[0].value
         addTodo({
             text,
             id:'text'+Math.random().toString(16).substr(2),
             checked:false
         })
     }
    return ( 
        <div>
            <h1>TODOS</h1>
            <Formulario onSubmit={handleSubmit}/>
            <Lista todo={todo} updateTodo={updateTodo} deleteTodo={deleteTodo}/>
        </div>
     );
}

const mapStateToProps =(state)=>{
    return{
        todo:state.todos
        
    }
}

const MapDispatchToProps = (dispatch) => {

    return {
        addTodo:(todo)=>dispatch(addTodo(todo)),
        updateTodo:(todo)=>dispatch(updateTodo(todo)),
        deleteTodo:(todo)=>dispatch(delteTodo(todo))
    };
}
 

export default connect(mapStateToProps,MapDispatchToProps)(Todos);