/*
 * @Author: Your name
 * @Date:   2020-04-01 16:42:21
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-01 16:51:03
 */
import Header from './Header_cf';
import React,{useState,useEffect,useDebugValue} from 'react';

const useSizes=()=>{
    const [width, setWidth] = useState(window.innerWidth);
    const [height, setHeight] = useState(window.innerHeight);

    useDebugValue("PrimerHook")

    const handleResize=()=>{
        setHeight(window.innerHeight)
        setWidth(window.innerWidth)
    }


    useEffect(() => {
        window.addEventListener("resize",handleResize)
        return () => {
            window.removeEventListener("resize",handleResize)
        };
    }, []);

    return({
        width,
        height
    })
}

const UseDebugValue = () => {
    const {width,height}=useSizes()
    
    return ( 
        <div>
            <Header title="HOOKS PERSONALIZADOS"/>
            <h1>
                width:{width}
            </h1>
            <h1>
            height: {height}
            </h1>
        </div>
     );
}
 
export default UseDebugValue;