/*
 * @Author: Your name
 * @Date:   2020-04-02 18:35:53
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-02 19:01:00
 */
import React, { Component } from 'react';
import  {Counter, Title, Button}  from './Componentes';


class ComposicionImplicita extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
            <Counter>
                <Button/>
                <Button type="increment"></Button>
                <Title>
                 {()=>(<div>titulo</div>)}
                </Title>
                <Title>
                    {(clicks)=>(<li>{clicks}</li>)}
                </Title>
            </Counter>
            </div>
         );
    }
}
 
export default ComposicionImplicita;