/*
 * @Author: Your name
 * @Date:   2020-03-29 22:49:15
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 05:21:40
 */
import React, { Component } from 'react';

const Header= ()=>{
    const subtitleStyle={
        fontWeight:'bold'
    }

    const headerStyle={
        margin:'0.6em',
        borderRadius:'0.3em',
        border:'1px solid #d2d2d2',
        padding:'2em 0.4em',
        fontFamily:'monospace',
        fontSize:'17px'
    }

    return(
        <header style={headerStyle}>
            <div>
                Comunicacion entre componentes
            </div>
        <div style={subtitleStyle}>
        Metodos de Instancia
        <span role="img" aria-label="flame">
            🔥
            
        </span>
        </div>
        </header>
    )
}

class Hijo extends Component {
    state = { 
        message:''
     }

    alerta=(e,message="HOLA DESDE EL HIJO")=>{
        alert(message)
        this.setState({ message });
    }
    render() { 
        return (
            <div>
            <h2>{this.state.message}</h2>
                <button onClick={this.alerta}>
                    HIjo
                </button>
            </div>
          );
    }
}

class Instancia extends Component {
    hijo = React.createRef()

    handleClick=()=>{
        this.hijo.current.alerta(null,"HOLA DESDE EL PADRE")
    }
    render() { 
        return (
            <div>
                <Header/>
                <Hijo ref={this.hijo}/>
                <button onClick={this.handleClick}>
                    Padre
                </button>
            </div>
          );
    }
}
 
export default Instancia;