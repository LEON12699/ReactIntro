/*
 * @Author: Your name
 * @Date:   2020-03-31 16:41:00
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 16:48:59
 */
import React, { Component } from 'react';

const HeaderCF = (props) => {
    const subtitleStyle = {
        fontWeight: "bold"
    };

    const headerStyle = {
        margin: "0.6em",
        color:"white",
        borderRadius: "0.3em",
        border: "1px solid #d2d2d2",
        padding: "2em 0.4em",
        fontFamily: "monospace",
        fontSize: "27px",
        textAlign: `center`,
        background: `radial-gradient(circle, rgba(217,15,23,1) 37%, rgba(17,19,20,1) 96%)`
    };

    const {title,subtitle}=props
    return (
        
        <header style={headerStyle}>
            <div>({title} )</div>
            <div style={subtitleStyle}>
            {subtitle}
        <span role="img" aria-label="flame">
                    🔥
        </span>
            </div>
        </header>
    );
};

export default HeaderCF