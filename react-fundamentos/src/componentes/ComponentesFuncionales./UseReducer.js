/*
 * @Author: Your name
 * @Date:   2020-03-31 22:51:39
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 23:12:15
 */
import React ,{useState,useReducer} from 'react';
import Header from './Header_cf';

// funcio  pura solo entra y sale , no se usa par asignar valores globales
const reducer=(state,action)=>{
    switch (action.type) {
        case "INCREMENTAR":
            return {...state,count:state.count+1}
        case "DISMINUIR":
            return {...state,count:state.count-1}
        case "SETTITLE":
            return {...state,title:action.title}
    
        default:
            return state
    }
    }



const UseReducer = () => {
  //  const [count, setCount] = useState(0);
  //  const [title, setTitle] = useState('Hola');
    
    const [state,dispatch]=useReducer(reducer,{
        count:0,
        title:'hola'
    })
    const increment=()=>{
        dispatch({type:"INCREMENTAR"})
    }

    const decrement=()=>{
        dispatch({type:"DISMINUIR"})
    }

    const handleTitle=(e)=>{
       // setTitle(e.target.value)
       dispatch({type:"SETTITLE",title:e.target.value})

    }

    return ( 
        <div>
            <Header
                title="Hook useReduce"
            />
            <input onChange={handleTitle} value={state.title}></input>
            <button onClick={increment}>
                INcrementar
            </button>
            <button onClick={decrement}>
                Disminuir
            </button>
            <h1>{state.count} - {state.title}</h1>
        </div>
     );
}
 
export default UseReducer;