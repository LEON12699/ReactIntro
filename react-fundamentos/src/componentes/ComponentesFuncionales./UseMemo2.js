/*
 * @Author: Your name
 * @Date:   2020-04-01 16:03:19
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-01 16:10:29
 */
import React, { useState,useMemo } from 'react';
import Header from './Header_cf'

const SuperList = ({list,log}) => {
  console.log("hola superlist"+log)
    return ( 
        <ul>
            {list.map(item=>(
                <li key={item.id}>
                    {item}
                </li>
            ))}
        </ul>
     );
}
 

const UseMemo2 = () => {

    const [cliks, setcliks] = useState(0);
    
    const memoList = useMemo(()=>{
        return(<SuperList
        list={[1,2,3,4,5,6,7]}
        log="memorizado"/>)
    },[])

    return ( 
        <div>
            <Header title="HOOk use memo"/>
            <SuperList
                list={
                    ['orange',
                    'pink',
                    'purple',
                    'yellow']}
                    log="normal"
            />
            <button onClick={()=>{
                setcliks(cliks+1)
            }}>
                clicks ({cliks})
            </button>
            {memoList}
        </div>
     );
}
 
export default UseMemo2;