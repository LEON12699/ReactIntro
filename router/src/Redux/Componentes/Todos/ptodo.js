/*
 * @Author: Your name
 * @Date:   2020-04-09 00:41:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:45:55
 */
import React from 'react';

const Ptodo = ({todo,updateTodo,deleteTodo}) => {
    return ( 
        <li  onClick={()=>{updateTodo(todo)}}>
                       <input type="checkbox"  onChange={()=>{
                           updateTodo(todo)
                       }} checked={todo.checked}/> {todo.text}
                        <button onClick={()=>deleteTodo(todo)}>
                            x
                        </button>
                    </li>
     );
}
 
export default Ptodo;