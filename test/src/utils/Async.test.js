/*
 * @Author: Your name
 * @Date:   2020-04-10 00:56:23
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 01:31:02
 */
import { getUsers ,getData,getDataPromise,getDataPromiseError} from './Async'

describe('operaciones asincronas', () => {
    
    test('Probando promesa con soliciyud http', async () => {
        const users = await getUsers()
        expect(users[0]).toHaveProperty('id')
    });

    test('Haciendo test a async await', async () => {
        const name = await getDataPromise()
        expect(name).toBe('israel leon')
    });

    test('Haciendo test a async await rechazada', async () => {
        try {
            const name = await getDataPromiseError()
            expect(name).toBe('israel leon')
        }catch(error){
            expect(error).toBe('error')
        }
    });

    test('Haciendo test a callback', (done) => {
        getData(name=>{
            expect(name).toEqual('israel leon');
            //expect(name).toBe('israel')
            done()
        })
    });

    test('Haciendo test a Promesas', (done) => {
        getDataPromise().then(name=>{
            expect(name).toEqual('israel leon');
            //expect(name).toBe('israel')
            done()
        }).catch(()=>{
            
        })
    });
    
        test('Haciendo test a Promesas con expect', () => {
            return expect(getDataPromise()).resolves.toBe('israel leon')
    });

    test('Haciendo test a Promesas error', (done) => {
        getDataPromiseError().then(name=>{
            
        }).catch((error)=>{
            expect(error).toEqual('error');
            //expect(name).toBe('israel')
            done()
        })
    });
    
        test('Haciendo test a Promesas con expect error', () => {
            return expect(getDataPromiseError()).rejects.toBe('error')
    });
});
