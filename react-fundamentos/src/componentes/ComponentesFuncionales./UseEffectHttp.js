
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 17:52:37
 */
import React, { useState, useEffect } from "react";
import HeaderCF from "./Header_cf";

const HookUseEffectHttp = () => {
    // retorna [value,function()]

    const [users,setUsers]=useState([])
    const [isfetching,setFetching]=useState(true)


    useEffect(()=>{
      fetch('https://jsonplaceholder.typicode.com/users')
      .then(res=>res.json())
      .then(usuarios=>{
        setUsers(usuarios)
        setFetching(false)
    })
    },[])
    // con array vacio se ejecuta una sola vez
    return ( 
        <div>
            <HeaderCF 
            title="Hook UseEffect http solicitud " 
            subtitle={isfetching?'Cargando...':'Users..'}/>
            <ul>
                {users.map(user=>(
                    <li key={user.id}>
                        {user.name}
                    </li>
                ))}
            </ul>
            
        </div>
     );
};

export default HookUseEffectHttp;
