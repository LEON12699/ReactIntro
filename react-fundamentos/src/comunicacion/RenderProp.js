/*
 * @Author: Your name
 * @Date:   2020-03-30 23:01:28
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 23:53:39
 */
import React, { Component } from "react";
import "./styles.css";
import Header from "../header";
//{provider,consumer}

class List extends Component {
    state = {  }
    render() { 
        const {list,render}=this.props
        return ( 
            <div>
                {list.map((item,index)=>{
                    if(render){
                        return render(item,index)
                    }
                    return(
                        <li key={item.name}>
                        {item.name}
                        </li>
                    )
                })}
            </div>
         );
    }
}
 


class RenderProps extends Component {
  state = {
    fruits:[
        {name:'Fresa',price:22},
        {name:'Mango',price:18},
        {name:'Sandia',price:24},
        {name:'Manzana',price:12},
    ]
  };

  render() {
    return (
     
        <div className="headerStyle">
          <Header 
          title="(Hijo a padre)" 
          subtitle="Render Props" />
          <List
          list={this.state.fruits}
          render={(item,index)=>(
              <div>
                  {item.name} - ${item.price}
              </div>
          )} />
        </div>

    );
  }
}

export default RenderProps;
