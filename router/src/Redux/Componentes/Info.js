/*
 * @Author: Your name
 * @Date:   2020-04-08 22:13:54
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:19:22
 */
import React from 'react';
import { connect} from 'react-redux'
import {} from '../Store'
import {updateName} from '../actions/userActions'
const Info = (props) => {
    const handleChange = (e) => {
        const name = e.target.value
        props.updateName(name)
    }
     
    return ( 
        <div>
            <h1>{props.user.name} - {props.user.country} </h1>
            <button onClick={props.updateName}>
                Actualizar name
            </button>
            <input type='text' onChange={handleChange}/>
        </div>
     );
}

const mapStateToProps =(state)=>{
    return{
        user:state.user,
        counter:state.counter
    }
}

const MapDispatchToProps = (dispatch) => {

    return {
        updateName:(name)=>dispatch(updateName(name))
    };
}
 

export default connect(mapStateToProps,MapDispatchToProps)(Info);