/*
 * @Author: Your name
 * @Date:   2020-03-27 00:01:10
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 00:07:52
 */
import React, { Component } from 'react';

const FancyInput=React.forwardRef((props,ref)=>(
    <div>
        <input type="text" ref={ref}/>
    </div>
))

class ReferenciasEnvio extends Component {
    entrada = React.createRef()

    componentDidMount(){
        console.log(this.entrada)
    }

    render() { 
        return ( 
            <div>
                <h1>Reenvio de Refs 🎠</h1>
                <FancyInput
                    ref={this.entrada}
                />
            </div>
         );
    }
}
 
export default ReferenciasEnvio;