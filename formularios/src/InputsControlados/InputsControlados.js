/*
 * @Author: Your name
 * @Date:   2020-03-27 00:52:37
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 01:17:20
 */
import React, { Component } from 'react';

class InputsC extends Component {
    state = { 
        text:"Mi practica",
        error:false,
        color:`#E8E8E8`
     }
     
     actualizar=(event)=>{
        const text=event.target.value
        //const error = (text!=='' && text.length<5) || text==''
        let color =`green`
        if(text.trim()===``){
            color=`E8E8E8`
        }
        if(text.trim()!==`` && text.trim().length<5){
            color=`red`
        }
        this.setState({ text,color});

    }

    render({text}=this.state) { 
        const styles={
            border: `1px solid ${this.state.color}`/*this.state.error?`1px solid red`:`1px solid #E8E8E8`*/,
            padding:`0.3em 0.6em`,
            outline:`none`
        }
        return ( 
            <input type="text"
            value={text}
            onChange={this.actualizar}
            style={styles}></input>
         );
    }
}
 



class InputsControlados extends Component {


    state = {  }
    render() { 
        return ( 
            <div>
                <h1>Inputs Controlados 💻</h1>
                <InputsC></InputsC>
            </div>
        );
    }
}
 
export default InputsControlados;