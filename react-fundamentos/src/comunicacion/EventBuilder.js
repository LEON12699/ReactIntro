/*
 * @Author: Your name
 * @Date:   2020-03-30 21:25:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 21:38:06
 */
import React, { Component } from 'react';


const Header= ()=>{
    const subtitleStyle={
        fontWeight:'bold'
    }

    const headerStyle={
        margin:'0.6em',
        borderRadius:'0.3em',
        border:'1px solid #d2d2d2',
        padding:'2em 0.4em',
        fontFamily:'monospace',
        fontSize:'17px'
    }

    return(
        <header style={headerStyle}>
            <div>
                (Hijo padre
               )
            </div>
        <div style={subtitleStyle}>
        Event builder
        <span role="img" aria-label="flame">
            🔥
            
        </span>
        </div>
        </header>
    )
                }
                
const boxStyle={
    margin:'0.6em',
    borderRadius:'0.3em',
    border:'1px solid blue',
    padding:'2em 0.4em',
    fontFamily:'monospace',
    fontSize:'17px'
    }

class Hijo extends Component {
    state = {  }
    handleCLick=(e)=>{
        //e.stopPropagation() // detiene la propagacion al siguiente componente lo anula al superior
        e.saludo="hola mensajendesde el hijo"
        console.log('click hijo')    
    }

    render() { 
        return ( 
            <div 
            style={boxStyle}
            onClick={this.handleCLick}>
                <p>HIJO</p>
            </div>
         );
    }
}
 




class EventBuilder extends Component {
  
    handleCLick=(e)=>{
        console.log('click <padre principal>',e.saludo)    
    }

    state = {  }
    render() { 
        return ( 
            <div
            onClick={this.handleCLick} 
            style={boxStyle}>
                <Header/>
                <Hijo/>
            </div>
         );
    }
}
 
export default EventBuilder