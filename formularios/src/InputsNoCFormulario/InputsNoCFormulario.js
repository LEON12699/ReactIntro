/*
 * @Author: Your name
 * @Date:   2020-03-27 00:41:16
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 17:37:05
 */
import React, { Component } from 'react';

const Emoji=()=>(
    <span
    role="img"
    aria-label="emoji">
    🌱
    </span>
)
class InputNoControlado extends Component{

    handleSubmit=(event)=>{
        event.preventDefault()
        const nombre = event.target[0].value
        const email = event.target[1].value
        
        //Manejar datos
        this.props.onSend({nombre,email})
    }

    render(){
        return(
            <form onSubmit={this.handleSubmit}>
                <p>
                <label htmlFor='name'>Nombre:</label>
                <input type="text" 
                placeholder="nombre"
                id="name"></input>
                </p>
                <p>
                <label htmlFor='email'>Email :</label>
                <input type="email"
                placeholder="email"
                id="email"></input>
                </p>
                <button >Enviar</button>
            </form>
        )
    }
}


class InputsNoCFormulario extends Component {
    send =(data)=>{
        console.log(data)
    }
    
    render() { 
        return ( 
            <div>
                <h1>Input no controlados Refs <Emoji></Emoji></h1>
                <InputNoControlado onSend={this.send}/>   
            </div>
         );
    }
}
 
export default InputsNoCFormulario;