/*
 * @Author: Your name
 * @Date:   2020-04-05 20:48:09
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-05 22:14:42
 */
import React, { useState } from 'react';
import './Carrusel.css'
import {TransitionGroup,CSSTransition} from 'react-transition-group'
import propTypes from 'prop-types'

const Componente = ({imagenes}) => {
    const [activeIndex, setActiveIndex] = useState(0);

    const handleNext =()=>activeIndex<imagenes.length-1?setActiveIndex(activeIndex+1):setActiveIndex(0)
    const handlePrevious=()=>activeIndex>0?setActiveIndex(activeIndex-1):setActiveIndex(imagenes.length-1)
    window.onkeydown=(ev)=>{
        if(ev.key=='ArrowRight'){
            handleNext()
        }
        else if(ev.key=='ArrowLeft'){
            handlePrevious()
        }
    }
    return ( 
        <div className='Carrousel'>
            <div className="Carrousel_Buttons">
            <button onClick={handlePrevious}>
                {"<--"}
            </button>
            
            </div>
            <TransitionGroup className="div">
                <CSSTransition
                timeout={1000}
                classNames='slide'
                key={activeIndex}>
                    <img className='Carrousel_img' src={imagenes[activeIndex]} alt="imagen activa"></img>        
                </CSSTransition>
            </TransitionGroup>
            <div className="Carrousel_Buttons_rigth">
            <button style={{background:'transparent',color:'white'} } onClick={handleNext}>
                -->
            </button>
            
            </div>
        </div>
     );
}
 

Componente.defaultProps={
    imagenes:[]
}

Componente.propTypes={
    imagenes:propTypes.arrayOf(
        propTypes.string
    )
}
const Carrusel = () => {
    return ( 
        <div>
            <Componente
                imagenes={
                    [
                        'https://images.pexels.com/photos/47367/full-moon-moon-bright-sky-47367.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                        'https://images.pexels.com/photos/861443/pexels-photo-861443.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                        'https://images.pexels.com/photos/556665/pexels-photo-556665.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',
                        'https://images.pexels.com/photos/1275413/pexels-photo-1275413.jpeg?auto=compress&cs=tinysrgb&dpr=1&w=500',

                    ]
                }
            />
        </div>
     );
}
 
export default Carrusel;