/*
 * @Author: Your name
 * @Date:   2020-03-31 23:14:38
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 23:25:43
 */
import React, { forwardRef, useImperativeHandle, useRef,useState } from 'react';
import Header from "./Header_cf"

const FancyInput = forwardRef((props,ref) => {
    const [text, setText] = useState('ás');
    const entrada = useRef()
    useImperativeHandle(ref,()=>({
        dispatchAlert:()=>{
            alert("HELLO")
        },
        setParagraph
    }))

    const setParagraph=(message)=>{
        setText(message)
    }

    const focus=()=>{
        entrada.current.focus()
    }
    return ( 
        <div>

            <input type="text" 
            value={text}
            ref={entrada}></input>
        </div>
     );
})


const UseImperativeHandle = () => {
    const FancyInputs =useRef()

    const handleClick=()=>{
        FancyInputs.current.setParagraph("hola")
    }

    return ( 
        <div>
            <Header title="useImperativeHandle"/>
            <FancyInput ref={FancyInputs}/>
            <button onClick={handleClick}>
                dispatch
            </button>
        </div>
     );
}
 
export default UseImperativeHandle;