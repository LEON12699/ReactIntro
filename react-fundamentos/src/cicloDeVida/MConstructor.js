/*
 * @Author: Your name
 * @Date:   2020-03-28 21:31:24
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 21:37:59
 */
import React, { Component } from 'react';


class Contador extends Component {
    constructor(props) {
        super(props);
        this.agregar=this.agregar.bind(this)
        this.state = { 
            message:`hola react`,
            num:this.props.num
         }
    }

    agregar(){
        this.setState({ 
           num :this.state.num+ 1  });
    }

    render() { 
        const {message,num}=this.state
        return ( 
            <div>
            <h2>{message}</h2>
            <button onClick={this.agregar}>
                Click ({num})
            </button>
            
            </div>

         );
    }
}

class MConstructor extends Component {
    constructor(props) {
        super(props);
        this.title=React.createRef()
       
    }

    

    render() { 
        return ( 
            <div>
            <h1 ref={this.title}>
            Metodo constructor
            </h1>
            <Contador num={2}/>

            
            </div>

         );
    }
}
 

export default MConstructor;