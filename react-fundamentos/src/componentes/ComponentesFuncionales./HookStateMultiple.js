
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 17:17:18
 */
import React, { useState } from "react";
import HeaderCF from "./Header_cf";

const HookStateMultiple= () => {
    // retorna [value,function()]

    const [clicks, setClicks] = useState(0)
    const [title, setTitle] = useState('Hola :)')


    
    const addClicks = () => {
        setClicks(clicks+1)
    };

    const handleInput = e => {
        const title = e.target.value;
        setTitle(title)
    };

    return (
        <div>
            <HeaderCF 
            title="Hook UseState -Objetos" 
            subtitle={title}>

            </HeaderCF>
            <input
                type="text"
                onChange={handleInput}
                value={title}>
            </input>
            <button onClick={addClicks}>CLicks ({clicks})</button>
        </div>
    );
};

export default HookStateMultiple
;
