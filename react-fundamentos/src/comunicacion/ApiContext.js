/*
 * @Author: Your name
 * @Date:   2020-03-30 23:01:28
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 23:34:02
 */
import React, { Component } from "react";
import "./styles.css";
import Header from "../header";
//{provider,consumer}

const { Provider, Consumer } = React.createContext();

class Hijo extends Component {
  state = {};
  render() {
    return (
      <div className="headerStyle">
        <p>Hijo</p>
        <Nieto />
      </div>
    );
  }
}

class Nieto extends Component {
  state = {};

  handleClick = () => {};

  render() {
    return (
      <Consumer>
        {(context) => {
          return (
            <div className="headerStyle">
              <p>NIETO</p>
              <button onClick={context.add}>
              Disparar ({context.clicks})</button>
            </div>
          );
        }}
      </Consumer>
    );
  }
}

class ApiContext extends Component {
  state = {
      clicks:0
  };

  addClicks=()=>{
    this.setState(state=>
    ({ clicks: state.clicks+1 })
    );
  }

  render() {
    return (
      <Provider value={{
        clicks:this.state.clicks,
        add:this.addClicks
      }}>
        <div className="headerStyle">
          <Header title="(Cualquiera)" subtitle="REACT API CONTEXT" />
          <Hijo />
        </div>
      </Provider>
    );
  }
}

export default ApiContext;
