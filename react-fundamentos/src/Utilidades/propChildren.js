/*
 * @Author: Your name
 * @Date:   2020-04-02 17:09:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-02 18:06:54
 */
import React, { Component } from 'react';
import HeaderCF from '../componentes/ComponentesFuncionales./Header_cf';
import './styles.css'

const Parent = ({children:ch}) => {

    const childrenArray = React.Children.toArray(ch)
    const children=React.Children.map(childrenArray,(child,index)=>{
        return <li>{child}</li>
    })

    return ( 
        <div className="box">
            <div className="box blue">
               {children}
               {/*React.children.only(children)} sirve para que solo reciva un children maximo*/}
            </div>
            <div className="box red">
               
            </div>
    
            
        </div>
     );
}
 

class PropChildren extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <HeaderCF title="PropChildren"/>
                <Parent>
                    HIjo de texto
                    <div>Elemento</div>
                    {()=>{}}
                    {444}
                    {`La suma es ${5+5}`}
                </Parent>
            </div>
         );
    }
}
 
export default PropChildren;