/*
 * @Author: Your name
 * @Date:   2020-04-08 23:24:07
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:40:34
 */
'use strict';

export const ADD_FRUIT='ADD_FRUIT'

export const addFruit=(fruit)=>{
    return {
        type:ADD_FRUIT,
        payload:{
            fruit
        }
    }
}