/*
 * @Author: Your name
 * @Date:   2020-03-30 22:01:25
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 22:24:37
 */
import React, { Component } from 'react';
import PubSub from 'pubsub-js'
/*
librerias que sirven para conectart culquiera hasta intermedios
    PubSUbjs
    EVENTeMMITER
    MicroEvent
*/



const headerStyle = {
    margin: "0.6em",
    borderRadius: "0.3em",
    border: "1px solid #d2d2d2",
    padding: "2em 0.4em",
    fontFamily: "monospace",
    fontSize: "17px",
    textAlign: `center`
};

const BlueStyle = {
    ...headerStyle,
    border: `3px solid blue`
};

const redStyle = {
    ...headerStyle,
    border: `3px solid red`
};

const Header = () => {
    const subtitleStyle = {
        fontWeight: "bold"
    };

    return (
        <header style={headerStyle}>
            <div>(Culquiera )</div>
            <div style={subtitleStyle}>
                Observer Pattern
        <span role="img" aria-label="flame">
                    🔥
        </span>
            </div>
        </header>
    );
};

class Hijo extends Component {
    state = {  }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Nieto/>
            </div>
         );
    }
}

class Nieto extends Component {
    state = {  }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Bisnieto/>
            </div>
         );
    }
}
class Bisnieto extends Component {
    state = { 
        message:'******'
     }

    componentWillUnmount(){
        PubSub.unsubscribe('otro')
    }

    componentDidMount(){
        PubSub.subscribe('otro',
        (e,data)=>{
            this.setState({ 
                message: data.title });
        })
    }
    handleCLick=()=>{
        PubSub.publish('saludo','Hola bisnieto')
    }

    render() { 
        return ( 
            <div style={headerStyle}>
               <p>{this.state.message}</p>
               <button onClick={this.handleCLick}>
                   Nieto
               </button> 
            </div>
         );
    }
}


class ObserverPattern extends Component {
    state = {  }

    componentDidMount(){
        PubSub.subscribe('saludo',
        (e,data)=>{
            alert(data)
        })
    }

    handleCLick=()=>{
        PubSub.publish('otro',{
            title:'Hola desde app'
        })
    }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Header/>
                <Hijo/>
                <button onClick={this.handleCLick}>
                    Padre
                </button>
            </div>
         );
    }
}
 
export default ObserverPattern;