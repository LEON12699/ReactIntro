/*
 * @Author: Your name
 * @Date:   2020-04-08 21:15:02
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 22:51:21
 */
import React from 'react';
import { connect} from 'react-redux'
import {increment,decrement} from '../actions/Counter'

const Counter = (props) => {
   /* const add=()=>{
       // props.dispatch(increment())
        
        props.increment()
    }

    const menos=()=>{
        //props.dispatch({type:'DECREMENT'})
        props.decrement()
    }
*/

    return ( 
        <div>
            <button onClick={props.increment}>+ </button>
            <button onClick={props.decrement}> -</button>
            <h1>{props.counter}</h1>
            <p>
                {props.name}
            </p>
        </div>
     );
}
 

const mapStateToProps =(state)=>{
    return{
        name:state.user.name,
        counter:state.counter
    }
}

const MapDispatchToProps = (dispatch) => {
    return {
        increment:()=>dispatch(increment()),
        decrement:()=>dispatch(decrement())
    };
}
 

export default connect(mapStateToProps,MapDispatchToProps)(Counter);