/*
 * @Author: Your name
 * @Date:   2020-03-31 23:28:21
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 23:53:48
 */
import React ,{useState}from 'react';
import Header from './Header_cf';

const Counter=React.memo(({count})=>{
    console.log('%crender counter','color:blue')
    return(
        <h1>
            {count}
        </h1>
    )
})

const Title=React.memo(({text})=>{
    console.log('%crender Title','color:oranged')
    return(
        <h1>
            {text}
        </h1>
    )
})

const TitleNested=React.memo(
    ({info})=>{
    console.log('%crender TitleNested','color:oranged')
    return(
        <h1>
            {info.title}
        </h1>
    )
    },
    (prevProps,nextProps)=>{
        // false se se renderiza true no
        
        return prevProps.info.title==nextProps.info.title
    }
    )

const Memo = () => {
    const [title, settitle] = useState('');
    const [count, setCount] = useState(0);

    const handleInput=(e)=>{
        settitle(e.target.value)
    }

    const handleAdd=()=>{
        setCount(count+1)
    }
    return ( 
        <div>
            <Header title="REACT.MEMO"/>
            <input
            type="text"
            onChange={handleInput}>

            </input>
            <button onClick={handleAdd}>
                add
            </button>
            <Counter count={count}/>
            <Title text={title}/>
            <TitleNested info={{title:title}}/>
        </div>
     );
}
 
export default Memo;