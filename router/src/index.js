/*
 * @Author: Your name
 * @Date:   2020-04-07 17:25:58
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 16:39:58
 */
import React from 'react';
import ReactDOM from 'react-dom';
import Props from './Props';
import Rutas from './Rutas';
import Switcha from './Switch';
import Redireccion from './Redireccion';
import AppR from './Redux/AppR';

/*

ReactDOM.render(
  <React.StrictMode>
    <Rutas />
  </React.StrictMode>,
  document.getElementById('root')
);


ReactDOM.render(
  <React.StrictMode>
    <Redireccion/>
  </React.StrictMode>,
  document.getElementById('root')
);*/

ReactDOM.render(
  <React.StrictMode>
    <AppR/>
  </React.StrictMode>,
  document.getElementById('root')
);
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
