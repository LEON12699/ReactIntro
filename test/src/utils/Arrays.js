/*
 * @Author: Your name
 * @Date:   2020-04-09 23:48:56
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:41:44
 */


export const CreateStore = () => {
    let fruits = []
    return {
        addFruit:(fruit)=>{
            fruits.push(fruit)
        },

        getFruits:()=>fruits
    };
}
 