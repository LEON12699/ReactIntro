/*
 * @Author: Your name
 * @Date:   2020-04-10 00:10:24
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:14:51
 */
import { CreateStore} from './Objetos'

describe('Test a  objetos', () => {
    
    test('Agregar un item store', () => {
        const store = CreateStore()

        store.additem({
            name:'Israel',
            id:1,
            country:'Ecuador'
        })

        expect(store.getStore()[0]).toEqual({
            name:'Israel',
            id:1,
            country:'Ecuador'
        })
    });
    
    test('Buscar  un item x id store', () => {
        const store = CreateStore()

        store.additem({
            name:'Israel',
            id:1,
            country:'Ecuador'
        })

        expect(store.getByid(1)).toEqual({
            name:'Israel',
            id:1,
            country:'Ecuador'
        })

        expect(store.getByid(1)).toMatchObject({
            name:'Israel'
        })

        expect(store.getByid(1)).toHaveProperty("name","Israel")
    });
});