/*
 * @Author: Your name
 * @Date:   2020-03-31 00:17:57
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 00:42:17
 */
import React, { Component } from 'react';
import "./styles.css";
import Header from "../header";

const withSizes =(Comp)=>
    class  extends Component {
        state={
            width:window.innerWidth,
            height:window.innerHeight
        }

        componentWillUnmount(){
            window.removeEventListener('resize',this.handleresize)
        }

        componentDidMount(){
            window.addEventListener('resize',this.handleresize)
        }

        handleresize=()=>{
            this.setState({ 
                width:window.innerWidth,
                height:window.innerHeight });
        }
     
        render() { 
            const {width,height}=this.state
            return ( 
                <Comp
                width={width}
                height={height}></Comp>
             );
        }
    }
     


class HOC extends Component {
    state = {  }
    render() { 
        const {width,height}=this.props
        return ( 
            <div className="headerStyle">
                <Header title="HIJO <-> PADRE"
                    subtitle="HOC High order Component 🔥"
                ></Header>
                <h1>{width}</h1>
                <h2>{height}</h2>
            </div>
         );
    }
}
 
export default withSizes(HOC);