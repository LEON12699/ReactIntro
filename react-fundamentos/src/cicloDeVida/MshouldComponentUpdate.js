/*
 * @Author: Your name
 * @Date:   2020-03-29 21:31:38
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-29 22:10:34
 */
import React, { Component ,PureComponent} from 'react';
const itemStyle={
    padding:'1em',
    borderBottom:'1px solid #CCC',
    marginTop:'0.4em'
    
}


class Item extends PureComponent {
    handleClick=()=>{
        this.props.onRemove(this.props.item)
    }

   /* shouldComponentUpdate(nextProps,nextState){
      
  //      if(nextProps.item.id!==this.props.item.id){
            return true
        }
        return false
    }*/
    

    render() { 
    const {item}=this.props

        return ( 
            <div style={itemStyle} >
            <button onClick={this.handleClick}>
                X
            </button>
            <span>{item.text}</span>
        </div>
         );
    }
}
 


class MshouldComponentUpdate extends Component {
    state = { 
       list:[]
     }

     add=(e)=>{
        e.preventDefault()
        const text=e.target[0].value
        const id=Math.random().toString(16)
        const pendiente={text,id}
        this.setState((state)=>({
             list:[...state.list,
            pendiente]  }));

        e.target[0].value=''     
        }

    shouldComponentUpdate(nextProps,nextState){
      return true
        
    }

    eliminar=(item)=>{
        this.setState({ 
           list:this.state.list.filter(_item=>{
                return item.id!==_item.id
            })  });
        console.log(item)
        }

    render() { 
        
        return ( 
            <div>
                <h2>shoudlComponentUpdate</h2>
                <form onSubmit={this.add}>
                    <input type="text" placeholder="ingresa pendiente"></input>
                    <button>
                        Agregar
                    </button>
                </form>
                <div>
                    {this.state.list.map(item=>(
                       <Item
                           key={item.id}
                           item={item}
                           onRemove={this.eliminar}
                       />
                    ))}
                </div>
            </div>
         );
    }
}
 
export default MshouldComponentUpdate;