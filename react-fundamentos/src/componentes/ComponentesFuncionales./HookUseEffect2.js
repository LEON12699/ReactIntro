
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 17:39:34
 */
import React, { useState, useEffect } from "react";
import HeaderCF from "./Header_cf";

const HookUseEffect2 = () => {
    // retorna [value,function()]

    const [clicks,setCLicks]=useState(0)
    const [emoji,setEmoji]=useState('🐶')

    const toogleEmoji=()=>{
        const nextEmoji= emoji==='🐶'?'🐒':'🐶'
        setEmoji(nextEmoji)
    }

    useEffect(()=>{
        alert("use effects")
    },[clicks])
    // con array vacio se ejecuta una sola vez
    return ( 
        <div>
            <HeaderCF 
            title="Hook UseEffects " subtitle={emoji}/>
            <button onClick={()=>{
                setCLicks(clicks+1)
            }}>
                Aumentar ({clicks})
            </button>
            <button onClick={toogleEmoji}>
                cambiar emoji
            </button>
            
        </div>
     );
};

export default HookUseEffect2;
