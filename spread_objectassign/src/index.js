/*
 * @Author: Your name
 * @Date:   2020-03-24 16:32:21
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-25 17:38:52
 */
import React, { Children } from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import GATOS from './Gatos';
import Mouse from './MouseEvent';
import Texto from './input';
import Persistencia from './EventosPersist';
import App_Test from './EventosPersonales';
import Condicional from './Condicional';
import Inyectado from './InyectadoHtml';
import HIJO from './Children';
import './destructuracion'
import Fragmentos from './fragments/Fragmentos';
import PortalModal from './Portales/PortalModal';
import Proptipes from './Proptipes';
/*
ReactDOM.render(
  <React.StrictMode>
    <App />
  </React.StrictMode>,
  document.getElementById('root')
);
*/
/*
ReactDOM.render(
  
    <GATOS />
  ,
  document.getElementById('root')
);

ReactDOM.render(
  
  <Mouse/>
,
document.getElementById('root')
);

ReactDOM.render(
  
  <Texto/>
,
document.getElementById('root')
);
ReactDOM.render(
  
  <AppTest/>
,
document.getElementById('root')
);

ReactDOM.render(
  
  <Condicional/>
,
document.getElementById('root')
);


ReactDOM.render(
  
  <Inyectado/>
,
document.getElementById('root')
);



ReactDOM.render(
  
  <HIJO/>
,
document.getElementById('root')
);



ReactDOM.render(
  
  <HIJO/>
,
document.getElementById('root')
);


ReactDOM.render(
  
  <Fragmentos/>
,
document.getElementById('root')
);
class App3 extends React.Component {
  
  state = {  
    visible:false,
    num :0
  }

  componentDidMount(){
    setInterval(()=>{
      this.setState(prevState => ({
        num:prevState.num+1
      }));
    },1000)
  }

  mostrar=()=>{
    this.setState({ visible: true });
  }

  cerrar=()=>{
    this.setState({ visible: false });
  }

  render() { 
    return (<div>
    <button onClick={this.mostrar}>Mostrar</button>
      <PortalModal saluda={this.state.visible}>
        <div>
        <button onClick={this.cerrar}>
          Cerrar
        </button>
        </div>
        HOLA DESDE un portalModal 👱 {this.state.num}
      </PortalModal>
      
    </div>  );
  }
}
 

 

ReactDOM.render(
  
  <App3/>
,
document.getElementById('root')
)

*/

ReactDOM.render(
  
  <Proptipes/>
,
document.getElementById('root')
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
