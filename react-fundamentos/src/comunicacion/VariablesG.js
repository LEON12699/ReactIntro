/*
 * @Author: Your name
 * @Date:   2020-03-30 22:35:15
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-30 23:00:02
 */
import React, { Component } from 'react';


const headerStyle = {
    margin: "0.6em",
    borderRadius: "0.3em",
    border: "1px solid #d2d2d2",
    padding: "2em 0.4em",
    fontFamily: "monospace",
    fontSize: "17px",
    textAlign: `center`
};

const BlueStyle = {
    ...headerStyle,
    border: `3px solid blue`
};

const redStyle = {
    ...headerStyle,
    border: `3px solid red`
};

const Header = () => {
    const subtitleStyle = {
        fontWeight: "bold"
    };

    return (
        <header style={headerStyle}>
            <div>(Culquiera )</div>
            <div style={subtitleStyle}>
                Observer Pattern
        <span role="img" aria-label="flame">
                    🔥
        </span>
            </div>
        </header>
    );
};

class Hijo extends Component {
    state = {  }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Nieto/>
            </div>
         );
    }
}

class Nieto extends Component {
    state = {  }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Bisnieto/>
            </div>
         );
    }
}
class Bisnieto extends Component {
    state = { 
        message:'******'
     }

    
   
    handleCLick=()=>{
        this.setState({ message: window.title });
    }

    render() { 
        return ( 
            <div style={headerStyle}>
               <p>{this.state.message}</p>
               <button onClick={this.handleCLick}>
                   Nieto
               </button> 
            </div>
         );
    }
}



class VariablesG extends Component {
    
    componentDidMount(){
       window.title='Developer con sueño'
    }

    handleCLick=()=>{
        window.title='padre'
    }
    render() { 
        return ( 
            <div style={headerStyle}>
                <Header/>
                <Hijo/>
                <button onClick={this.handleCLick}>
                    Padre
                </button>
            </div>
         );
    }
}
 
export default VariablesG;