/*
 * @Author: Your name
 * @Date:   2020-03-25 23:01:17
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 00:02:15
 */
import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './App';
import * as serviceWorker from './serviceWorker';
import Lista1 from './Lista1';
import ListaObjetos from './ListaObjetos';
import ListaObjetosProp from './ListaObjetosProp';
import PropKey from './PropKey';
import IterComponentes from './IterComponentes';
import PasarDataIter from './PasarDataIter';
import Referencias from './Referencias';
import ReferenciasTerceros from './ReferenciasTercero';
import ReferenciasEnvio from './ReferenciasEnvio';
/*
ReactDOM.render(
  <Lista1/>,
  document.getElementById('root')
);


ReactDOM.render(
  <div style={{margin:`1em`}}>
  <ListaObjetos/>
  </div>,
  document.getElementById('root')
);

ReactDOM.render(
  <ListaObjetosProp/>
  ,document.getElementById('root')
)


ReactDOM.render(
  <PropKey/>
  ,document.getElementById('root')
)


ReactDOM.render(
  <IterComponentes/>
  ,document.getElementById('root')
)


ReactDOM.render(
  <PasarDataIter/>
  ,document.getElementById('root')
)

ReactDOM.render(
  <Referencias/>
  ,document.getElementById('root')
)


ReactDOM.render(
  <ReferenciasTerceros/>
  ,document.getElementById('root')
)*/

ReactDOM.render(
  <ReferenciasEnvio/>
  ,document.getElementById('root')
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
