/*
 * @Author: Your name
 * @Date:   2020-03-24 22:17:03
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 22:31:31
 */
import React,{ Component } from "react";


class Persistencia extends Component{
    state={
        color:"blue"
    }

    handlerChange=  (event)=>{
        const color=event.target.value 
        this.setState(state=>({
            color
        }))
    }

    render(){
        return(
            <div>
                <input type="text"
                onChange={this.handlerChange}></input>
                <h1 style={{color:this.state.color}}>
                    {this.state.color}
                </h1>
            </div>
        )
    }
}

export default Persistencia