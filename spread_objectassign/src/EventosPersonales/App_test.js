/*
 * @Author: Your name
 * @Date:   2020-03-24 22:30:28
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 22:49:20
 */
import React, { Component } from "react";
import "./css.css"
class Hijo extends Component{
    saluda=()=>{
        this.props.onSaluda("React ⏰")
    }

    render(){
        return(<div className="box red">
            <h2>Hijo</h2>
            <button
            onClick={this.saluda}>Saluda</button>
        </div>)
    }
}

class AppTest extends Component{
    state={
        name:""
    }

    manejador=(name)=>{
        this.setState(
        state=>({
            name
        })
        )
    }
        
    

    render(){
        return(
            <div className="box purple">
            <Hijo onSaluda={this.manejador}></Hijo>                
           <h1>Nombre :{this.state.name}</h1>
           </div>
        )
    }
}

export default AppTest