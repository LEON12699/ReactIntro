/*
 * @Author: Your name
 * @Date:   2020-03-24 23:26:49
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 23:42:27
 */
import React, { Component } from "react";
import ReactDOM from 'react-dom';

class Inyectado extends Component{
    state={
        marcado:`
        <h1> Inyeccion de marcado</h1>
        <hr/>
        <hr/>
        <a href="#" >Link</a>
        `
    }

    render(){
        return(
            <div>
                <div
                dangerouslySetInnerHTML={{ __html:this.state.marcado}}
                ></div>
            </div>
        )
    }
}

export default Inyectado