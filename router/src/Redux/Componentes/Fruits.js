/*
 * @Author: Your name
 * @Date:   2020-04-08 22:13:54
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:44:33
 */
import React from 'react';
import { connect} from 'react-redux'
import {} from '../Store'
import {addFruit} from '../actions/FruitActions'


const Fruits = ({fruits,addFruit}) => {
    
    const handlerSubmit = (e) => {
    
        e.preventDefault()
        const name= e.target[0].value
        addFruit(name)
    }
     
    
    return ( 
        <div>   
        <h1>FRUITS</h1>
        <form onSubmit={handlerSubmit}>
            <input type="text" placeholder="agrega fruta"></input>
            <button>Agregar</button>
        </form>
            <ul>
                {fruits.map((fruit,index)=>(
                    <li key={fruit+index}>
                        {fruit}
                    </li>
                ))}
            </ul>
        </div>  
     );
}

const mapStateToProps =(state)=>{
    return{
        fruits:state.fruits
        
    }
}

const MapDispatchToProps = (dispatch) => {

    return {
        addFruit:(fruit)=>dispatch(addFruit(fruit
            ))
    };
}
 

export default connect(mapStateToProps,MapDispatchToProps)(Fruits);