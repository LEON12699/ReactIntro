/*
 * @Author: Your name
 * @Date:   2020-04-09 01:26:10
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 01:51:05
 */

import { FECTH_POSTS_ERROR,FECTH_POSTS_REQUEST,FECTH_POSTS_SUCCESS} from '../actions/BlogActions'
const inicial={
    posts:[],
    isFetching:false,
    error:null
}

function posts(state=inicial,action){
    switch (action.type) {

        case FECTH_POSTS_REQUEST:
            return {
                ...state,
                isFetching:true
            }
        case FECTH_POSTS_SUCCESS:
            return {
                ...state,
                isFetching:false,
                posts:action.payload.posts
            }
        case FECTH_POSTS_ERROR:
            return {
                ...state,
                isFetching:false,
                error:action.payload.error
            }
        default:
            return state
    }
}

export default posts