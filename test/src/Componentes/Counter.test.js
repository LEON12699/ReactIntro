/*
 * @Author: Your name
 * @Date:   2020-04-10 02:33:57
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:43:13
 */
import React from 'react';
import Enzyme,{shallow,mount,render,toMatchSnapshot} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import List from './List'
import COunter from './Counter';

Enzyme.configure({ adapter: new Adapter() });

describe('Probando componentes COunter', () => {
    let wraper 

    beforeEach(()=>{
        wraper= shallow(<COunter/>)
    })

    test('validar boton', () => {
        wraper.find('button').first().simulate('click')
        expect(wraper.find('h1').text()).toBe('1')
    });
    
  /*  test('Validar que coincida con Snapshot', () => {
        expect(wraper.html()).toMatchSnapshot()
    });*/

   
    
});