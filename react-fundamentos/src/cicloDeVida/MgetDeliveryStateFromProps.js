/*
 * @Author: Your name
 * @Date:   2020-03-29 21:16:21
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-29 21:27:32
 */
import React, { Component } from 'react';

class Contador extends Component {
    state = { 
        num:this.props.num
     }

     static getDerivedStateFromProps(nextProps,prevState){
       if(prevState.num<nextProps.num){
           return{
               num:nextProps.num
           }
       }
  
     }

     add=()=>{
        this.setState(state=>({
            num:state.num+1
        }));
     }

    render() { 
        const{num}=this.state
        return (  
            <div>
                <hr/>
                <button
                onClick={this.add}>
                    CLicks ({num})
                </button>
            </div>
        );
    }
}
 


class MgetDeliveryStateFromProps extends Component {
    state = { 
        numero:0
     }
    handleChange=(e)=>{
        var numero=parseInt(e.target.value)
        if(isNaN(numero)){
            numero=0
        }

        this.setState({ numero  });
    }

    render() { 
        const {numero}=this.state
        return ( 
            <div>
                <h1>getDeliveryStateFromProps</h1>
                <h2>{numero}</h2>
                <input
                type="text"
                onChange={this.handleChange}>

                </input>
                <Contador num={numero}/>
            </div>
         );
    }
}
 
export default MgetDeliveryStateFromProps;