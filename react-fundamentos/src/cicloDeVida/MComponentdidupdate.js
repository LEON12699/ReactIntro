/*
 * @Author: Your name
 * @Date:   2020-03-28 22:26:56
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 23:01:36
 */
import React, { Component } from 'react';

class UserD extends Component {
    state = { 
        user:{},
        carga:false
     }

     fetchdata=()=>{
         this.setState({ carga: true });
        fetch('https://jsonplaceholder.typicode.com/users/'+this.props.userId)
        .then(res=>res.json())
        .then(user=>this.setState({ user,carga:false  }))
     }
     componentDidMount(){
         this.fetchdata()
         
     }

     componentDidUpdate(prevProps,prevState){
         if(prevProps.userId!== this.props.userId){
            this.fetchdata()
         }

     }

    render() { 
        const {user} =this.state
        return ( 
            <div>
                <h2>User detail</h2>
                { this.state.carga
               ?<h1>Cargando...</h1>:(<pre>
                   {JSON.stringify(user,null,4)}
               </pre>)
                }
            </div>
        );
    }
}

class MComponentdidupdate extends Component {
    state = { 
        id:1
     }

     aumenta=()=>{
        this.setState(state=>({
            id:state.id+1
        }));
     }

    render() { 
        const {id}=this.state
        return ( 
            <div>
                <h1>ComponentDidUpdate</h1>
                <h2>id {id}</h2>
                <button onClick={this.aumenta}>
                    Aumentar
                </button>
                <UserD userId={id}></UserD>
            </div>
         );
    }
}
 
export default MComponentdidupdate;
