/*
 * @Author: Your name
 * @Date:   2020-04-08 23:54:20
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:30:17
 */

import {ADD_TODO,UPDATE_TODO,DELETE_TODO} from '../actions/TodoActions'

const inicial={
    todos:[
    {
        text:'Crear componente',
        id:'a12312a',
        checked:false
    },
    {
        text:'subir videos',
        id:'a1231sa',
        checked:true
    }
    ]
}


function todo (state=inicial,action){
    switch (action.type) {
       case ADD_TODO:
           return{
            ...state,
            todos:[
                ...state.todos,
                action.payload
            ]
           }
        case UPDATE_TODO:
           return{
               ...state,
               todos:[
                   ...state.todos.map((todo)=>{
                       if(action.payload.id==todo.id){
                           return{
                               ...todo,
                               checked:!todo.checked
                           }
                       }
                       return todo
                   })
               ]
           }
        case DELETE_TODO:
            return {
                ...state,
                todos:state.todos.filter(
                    (todo)=>action.payload.id !==todo.id
                )
            }
        default:
            return state;
    }
}

export default todo