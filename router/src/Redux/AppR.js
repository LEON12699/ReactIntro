/*
 * @Author: Your name
 * @Date:   2020-04-08 16:36:50
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 01:29:55
 */
import React from 'react';
import { Provider }  from 'react-redux'
import store from './Store'
import Counter from './Componentes/Counter';
import Info from './Componentes/Info';
import Fruits from './Componentes/Fruits';
import Todos from './Componentes/Todos/todos'
import Controller from './Componentes/Controller';
import Blog from './Componentes/Blog';

const AppR = () => {
    return (  
        <Provider store={store}>
        <div>
            {/*<Fruits></Fruits>
            <Counter/>
            <Info/>
            <Todos/>
            <Controller/>*/}
            <Blog/>
        </div>
        </Provider>
    );
}
 
export default AppR;