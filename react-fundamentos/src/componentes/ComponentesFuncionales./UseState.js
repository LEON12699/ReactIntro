/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 16:54:09
 */
import React, { Component,useState } from 'react';
import HeaderCF from './Header_cf';


const UseState=()=>{
    // retorna [value,function()]
    
    const [clicks,setCLicks]=useState(0)
    



    return ( 
        <div>
            <HeaderCF 
            title="Hook UseState "/>
            <button onClick={()=>{
                setCLicks(clicks+1)
            }}>
                CLICKS ({clicks})
            </button>
        </div>
     );
}

export default UseState;