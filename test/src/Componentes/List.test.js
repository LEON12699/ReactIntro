/*
 * @Author: Your name
 * @Date:   2020-04-10 01:38:05
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:29:06
 */

 /* shallow test a componentes como una sola unidad cada uno tiene su contexto
 
 mount para interactuar con apis del dom o componentes envueltos
 lo mas parecido a un entorno de navegador el contexto es global

 render transforma el arbol en un marcado html y lo envuelve en cheerio 
 similar a jquery para interacciones

 **/
import React from 'react';
import Enzyme,{shallow,mount,render,toMatchSnapshot} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import List from './List'

Enzyme.configure({ adapter: new Adapter() });

describe('probando componente list', () => {
    const fruits =[
        {name:"fresa",id:1},
        {name:"fresas",id:2},
    ]
    let wraper
    
    beforeEach(()=>{
        wraper=shallow(<List title="frutas" lista={fruits}/>)
    })

        test('validar nodos', () => {
            expect(wraper.find('h1').exists()).toBe(true)
            
            // validar si hay css
            expect(wraper.find('h1').hasClass('big')).toBe(false)

            // valdiar antidad de elementos que tiene
            expect(wraper.find('ul').children().length).toBe(2)

            //childAt se elije el padre(ul) y se elije el numero de nodo a acceder
            expect(wraper.find('ul').childAt(1).text()).toBe('fresas')
            
            expect(wraper.find('ul').childAt(1).type()).toBe('li')

        });

        test('validar actualizacipnes props', () => {
            
            expect(wraper.find('li').length).toBe(2)

            wraper.setProps({
                lista:[
                    {name:"Mango",id:5}
                ]
            })

            expect(wraper.find('li').length).toBe(1)

        });


        test('validar snapshot', () => {
            expect(wraper.html()).toMatchSnapshot()
        });
        
        
        
});