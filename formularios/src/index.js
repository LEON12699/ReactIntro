/*
 * @Author: Your name
 * @Date:   2020-03-27 00:21:48
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 22:01:12
 */
import React from 'react';
import ReactDOM from 'react-dom';
import InputsNoControl from './InputsNoControl';
import InputsNoCFormulario from './InputsNoCFormulario/InputsNoCFormulario';
import InputsControlados from './InputsControlados';
import InputReutilizable from './InputReutilizable';
import Selects from './Selects';
import INputCheckbox from './InputCheckbox';
import Errores from './Errores';
//import './index.css';
//import App from './App';
//import * as serviceWorker from './serviceWorker';
/*
const App =()=>(
  <div>
    <h1>inicio</h1>
  </div>
)

ReactDOM.render(
  <React.StrictMode>
    <InputsNoControl/>
  </React.StrictMode>,
  document.getElementById('root')
);


ReactDOM.render(
  <React.StrictMode>
    <InputsNoCFormulario/>
  </React.StrictMode>,
  document.getElementById('root')
);
*/
/*
ReactDOM.render(
  <React.StrictMode>
    <InputsControlados/>
  </React.StrictMode>,
  document.getElementById('root')
);


ReactDOM.render(
  <React.StrictMode>
    <InputReutilizable/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <Selects/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <INputCheckbox/>
  </React.StrictMode>,
  document.getElementById('root')
)

*/

ReactDOM.render(
  <React.StrictMode>
    <Errores/>
  </React.StrictMode>,
  document.getElementById('root')
)
// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
//serviceWorker.unregister();
