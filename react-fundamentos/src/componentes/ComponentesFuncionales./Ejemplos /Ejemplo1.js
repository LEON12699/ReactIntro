
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 22:39:43
 */
import React, { useState,useEffect,useRef } from 'react';
import HeaderCF from './../Header_cf';



const Ejemplo1 = () => {
    // retorna [value,function()]
    const entrada = useRef()
    const [ products, setProducts ] = useState([])
    const [name,setName]=useState('')

    const handleInput=(e)=>{
        setName(e.target.value)
    }   

    useEffect(()=>{
        setTimeout(()=>{
            if(name===entrada.current.value){
            fetch('https://universidad-react-api-test.luxfenix.now.sh/products?name='+name)
            .then(res => res.json())
            .then(data => setProducts(data.products))

            }
        },600)
        
    },[name])

    return (

        <div>
            <HeaderCF
                title="Hook useRef Ejemplos"
            />
            <input
            type="text"
            onChange={handleInput}
            ref ={entrada}/>
            <ul>
                {products.map(product=>(
                    <li key={product.id}>
                        {product.name}
                    </li>
                ))}
            </ul>
            
        

        </div>

    );
}

export default Ejemplo1
    ;