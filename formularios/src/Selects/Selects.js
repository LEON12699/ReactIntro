/*
 * @Author: Your name
 * @Date:   2020-03-27 17:40:42
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 17:53:08
 */
import React, { Component } from 'react';

const Emoji=()=>(
    <span
    role="img"
    aria-label="label">
    🙋
    </span>
)
class SelectsMultiple extends Component {
    state = { 
        techs:[`Vue`]
     }

     handleChange=(event)=>{
        const techs = Array.from(
            event.target.selectedOptions,
            (option)=>option.value

        )
        this.setState({ techs  });
     }

    render() { 
        return (    <div>
            <h1>
                Selects Multiple <Emoji/>
                
            </h1>
            <form>
                <select value={this.state.tech} multiple onChange={this.handleChange}>
                    <option value="Angular">Angular</option>
                    <option value="React">React</option>
                    <option value="Vue">Vue</option>
                    <option value="Vainilla">Vainilla</option>
                
                </select>
            
            </form>
            <ul>
                {this.state.techs.map(tech=>
                    (
                        <li key={tech}>{tech}</li>
                    )
                )}
            </ul>
        </div> );
    }
}
 


class Selects extends Component {
    state = {  
        tech:`Vue`
     }

     handleChange=(event)=>{
        this.setState({ 
            tech : event.target.value });
     }
    render() { 
        return ( 
            <div>
                <h1>
                    Select <Emoji/>
                    {this.state.tech}
                </h1>
                <form>
                    <select value={this.state.tech} onChange={this.handleChange}>
                        <option value="Angular">Angular</option>
                        <option value="React">React</option>
                        <option value="Vue">Vue</option>
                        <option value="Vainilla">Vainilla</option>
                    
                    </select>
                </form>
                <SelectsMultiple/>
            </div>
         );
    }
}
 
export default Selects;