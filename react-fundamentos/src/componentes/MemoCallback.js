/*
 * @Author: Your name
 * @Date:   2020-03-31 23:57:10
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-01 15:58:05
 */
import React, { useState,useCallback } from 'react';
import Header from '../header';


const getRandomColor=()=>'#'+Math.random().toString(16).slice(2,8)
const Button=React.memo(({callback,children})=>{
    const styles={
        padding:'1em',
        fontSize:'19px',
        background:getRandomColor()
    }

    return(
        <div>
            <button style={styles} onClick={callback}>
                {children}
            </button>
        </div>
    )
})
const MemoCallback = () => {
    const [A, setA] = useState(0);
    const [B, setB] = useState(0);
    
    const IncrementaA=useCallback(()=>{
        setA(A=>A+1)
    },[])

    const IncrementB=useCallback(()=>{
        setB(B=>A+B)
    },[A])
    
    return ( 
        <div>
            <Header title="hook use Calback"/>
            <Button callback={IncrementaA}>
                INcrementa A
            </Button>
            <Button callback={IncrementB}>
                INcrementa B
            </Button>
            {A} {B}
        </div>
     );
}
 
export default MemoCallback;