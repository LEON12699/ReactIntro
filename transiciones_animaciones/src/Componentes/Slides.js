/*
 * @Author: Your name
 * @Date:   2020-04-05 00:47:46
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-05 01:56:23
 */
import React,{useState,useEffect}from 'react';
import './slides.css'
import propTypes from 'prop-types'
const Componente = ({imagenes, interval}) => {

    const [activeIndex, setActiveIndex] = useState(0);
   


    useEffect(() => {
        const tick=setInterval(()=>{
            if(activeIndex<imagenes.length-1){
                setActiveIndex(activeIndex+1)
            }else{
                setActiveIndex(0)
            }
        },interval)
        return () => {
            clearInterval(tick)
        };
    }, [activeIndex,imagenes.length,interval]);
    return ( 
        <div className="Slide">
            <div className="Slide_container">
                {imagenes.map((image,index)=>(
                    <img
                    key={image.src}  
                    src={image.src} 
                    className={
                        index===activeIndex
                        ?'imagen animate-show'
                        :'imagen animate-hide'
                    }
                    alt={image.title}></img>
                    
                ))}
               <div className="Slide_titulo">
                   {imagenes[activeIndex].title}
               </div>
            
            </div>
        </div>
     );
}
 
Componente.defaultProps={
    interval:4000,
    imagenes:[
        {
            src:'https://images.pexels.com/photos/3757147/pexels-photo-3757147.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            title:'avion'
        },
        {
            src:'https://images.pexels.com/photos/3768577/pexels-photo-3768577.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            title:'chica'
        },
        {
            src:'https://images.pexels.com/photos/3326365/pexels-photo-3326365.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            title:'playa'
        },
        {
            src:'https://images.pexels.com/photos/2793456/pexels-photo-2793456.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940',
            title:'carretera'
        }
    ]
}

Componente.propTypes={
    interval:propTypes.number,
    imagenes:propTypes.arrayOf(
        propTypes.shape({
            src:propTypes.string.isRequired,
            title:propTypes.string.isRequired
        })
    )
}

const Slides = () => {
    return ( 
        <div>
            <Componente/>
        </div>
     );
}
 
export default Slides;