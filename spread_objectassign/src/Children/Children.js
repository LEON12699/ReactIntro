/*
 * @Author: Your name
 * @Date:   2020-03-24 23:46:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 23:58:02
 */
import React  from "react";

const Title = (props) => {
    const styles={
        padding:`0.3em`,
        color:`#FFF`,
        background:props.uiColor,
        borderRadius:`0.3em`,
        textAlign:`center`,
        fontSize:`50px`
    }
    return ( <h1 style={styles}>{props.children}</h1> );
}
 


class HIJO extends React.Component {
    state = {  
        uiColor:'purple'
    }
    render() { 
        return (
            <div>
                <Title uiColor={this.state.uiColor}>
                    Super <em>Children</em>
                </Title>
            </div>
        );
    }
}
 
export default HIJO;