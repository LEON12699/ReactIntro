
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 17:30:27
 */
import React, { useState, useEffect } from "react";
import HeaderCF from "./Header_cf";

const HookUseEffect = () => {
    // retorna [value,function()]

    const [mouseX, setMouseX] = useState(0)
    const [mouseY, setMouseY] = useState(0)

    // componentDidMount
    //componentDidUpdate
    //componentWIilUnmount
    useEffect(() => {
        //componentDidMount
        window.addEventListener('mousemove', handlemove)
        //componentDidUpdate
        return ()=>{
            window.removeEventListener('mousemove', handlemove)

        }
    });

    const handlemove = (e) => {
        setMouseX(e.clientX)
        setMouseY(e.clientY)
    }





    return (
        <div>
            <HeaderCF
                title="Hook Use Effect "
            >

            </HeaderCF>
            <h1>
                x: {mouseX} Y: {mouseY}
            </h1>
        </div>
    );
};

export default HookUseEffect
    ;
