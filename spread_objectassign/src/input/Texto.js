/*
 * @Author: Your name
 * @Date:   2020-03-24 21:50:18
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 22:04:51
 */
import React,{ Component } from "react";

class Texto extends Component{
    state={
        text:"",
        evento:""
    }

    manejador=(event)=>{
        console.log(event)
        this.setState({
            
            text:event.target.value,
            evento:event.type
        })
    }

    render(){
        return(
            <div>
                <input type="text"
                onChange={this.manejador}
                onCopy={this.manejador}
                onPaste={this.manejador}></input>
                
                <h2>{this.state.text}</h2>
                <h3>{this.state.evento}</h3>
            </div>
        )
    }
    
}

export default Texto