/*
 * @Author: Your name
 * @Date:   2020-04-04 18:52:48
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-05 00:29:37
 */
import React, { useState ,useRef} from 'react';
import imagen from '../image/arrow.svg'
import propTypes from 'prop-types'


const Componente = ({title,content,bgColor}) => {
    const [expanded, setExpanded] = useState(false);
    const contentRef = useRef()

    const panelStyle={
        background:bgColor,
        color:'#FFF',
        padding:'0.5em 1em',
        display:'flex',
        alignItems:'center',
        justifyContent:'space-between',
        userSelect:'none'
    }

    const contentStyle={
        height:expanded?contentRef.current.scrollHeight:'0px',
        overflow:'hidden',
        transition:'all 550ms ease-out',
        border:'1px solid '+bgColor,
        padding:expanded?'1em 0.5em':'0 0.5em'
    }


    const toogle=()=>{
        setExpanded(!expanded)
    }

    const imagenStyles={
        width:'18px',
        transition:'transform 300ms ease',
        transform:expanded?'rotate(180deg)':'rotate(0deg)'
        

    }



    return ( 
        <div >
            <div style={panelStyle} onClick={toogle}>
                <span>{title}</span>
                <img
                    src={imagen}
                    alt="flecha hacia abajo"
                    style={imagenStyles}>

                </img>
            </div>
            <div ref={contentRef} style={contentStyle}>
                {content}
            </div>
        </div>
     );
}
 
Componente.defaultProps={
    title:'titulo',
    content:'contenido ...',
    bgColor:'radial-gradient(black  3px, transparent 0.5px) 30px 30px / 15px 15px rgb(87, 0, 145)'
}

Componente.propTypes={
    title:propTypes.string,
    content:propTypes.object,
    bgColor:propTypes.string
}

const Acordeon = () => {
    return ( 
        <div>
            <Componente
                title="Ejemplo acordeon"
                content={<p>'un contenido <br></br>dinamico'</p>}
            />
            <Componente
                content={<div>
                    <table>
                        <theader>
                            <td>
                                <th>Una</th>
                                <th>dos</th>
                            </td>
                        </theader>
                        <tbody>
                            <td>
                                <th>1</th>
                            </td>
                        </tbody>
                    </table>
                </div>}

                bgColor="#f58924"
            />
            
        </div>
     );
}
 
export default Acordeon;