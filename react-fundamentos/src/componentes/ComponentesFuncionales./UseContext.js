
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-01 16:03:58
 */
import React, { useState,useContext} from 'react';
import HeaderCF from './Header_cf';


const Mycontext =React.createContext() 

/* FOrma tradicional context
const Nieto=()=>(
    <Mycontext.Consumer>
   {(context)=>( <div>
        <p>NIeto {context.clicks}</p>
        <button onClick={context.add}>
            Nieto Dispacher
        </button>
    </div>)}
    </Mycontext.Consumer>
)
*/
const Nieto=()=>{
    const context=useContext(Mycontext)
    
    return (
    <div>
        <p>NIeto {context.clicks}</p>
        <button onClick={context.add}>
            Nieto Dispacher
        </button>
    </div>)
    }


const Hijo=()=>(
    <div>
        <p>HIjo</p>
        <Nieto/>
    </div>
)

const UseContext=()=>{
    // retorna [value,function()]
    
    const [clicks,setCLicks]=useState(0)
    
    const add=()=>{
        setCLicks(clicks+1)
    }
    
    return ( 
        <Mycontext.Provider value={{
            clicks,
            add
        }}>
        <div>
            <HeaderCF 
            title="Hook UseCOntext "/>
            <button onClick={add}>
                CLICKS ({clicks})
            </button>
            <Hijo/>
        </div>
        </Mycontext.Provider>
     );
}

export default UseContext
;