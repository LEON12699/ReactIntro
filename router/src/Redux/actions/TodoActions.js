/*
 * @Author: Your name
 * @Date:   2020-04-09 00:11:20
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:27:43
 */
export const ADD_TODO='ADD_TODO'
export const DELETE_TODO='DELETE_TODO'
export const UPDATE_TODO='UPDATE_TODO'


export const addTodo=(todo)=>({
    type:ADD_TODO,
    payload:todo
})

export const updateTodo=(todo)=>({
    type:UPDATE_TODO,
    payload:todo
})

export const delteTodo=(todo)=>({
    type:DELETE_TODO,
    payload:todo
})