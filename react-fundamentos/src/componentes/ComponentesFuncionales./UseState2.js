/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 16:59:14
 */
import React, { useState } from 'react';
import HeaderCF from './Header_cf';


const UseState2=()=>{
    // retorna [value,function()]
    
    const [isActive,setActive]=useState(false)
    



    const toogle=()=>{
        setActive(!isActive)
    }

    return ( 
        <div>
            {isActive && <HeaderCF 
            title="Hook UseState "/>}
            <button onClick={toogle}>
                {isActive?`desactivar`:`activar`}
            </button>
        </div>
     );
}

export default UseState2;