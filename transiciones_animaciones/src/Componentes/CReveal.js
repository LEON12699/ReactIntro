/*
 * @Author: Your name
 * @Date:   2020-04-05 22:42:26
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-05 23:01:52
 */
import React from 'react';
import Zoom from 'react-reveal/Zoom';
import Bounce from 'react-reveal/Bounce';
import Rotate from 'react-reveal/Rotate';

const CReveal = () => {
    return (
        <div>
        <Zoom>
            <section>
                <h3>Ejemplo de titulo</h3>
                <p>
                    Exercitation id occaecat laboris velit ipsum duis laborum incididunt. Cillum sit deserunt proident nostrud minim laboris ullamco exercitation et culpa. Sunt irure cillum aliqua enim est minim consectetur ut nisi deserunt quis dolore. Non ut commodo ullamco do. Duis aliquip dolore incididunt aliqua enim velit ullamco.

                    Esse proident ut amet mollit ipsum aliquip do deserunt. Aute qui officia aliquip ipsum id ullamco laboris ex do cillum cupidatat nulla. Aute Lorem excepteur esse veniam.

                    Deserunt id proident id dolore tempor culpa incididunt officia cillum quis. Id magna irure ea velit est sunt consequat. Velit in proident labore ex labore Lorem aliqua exercitation. Irure pariatur ut cupidatat labore anim velit in aliqua fugiat Lorem commodo. Et fugiat est irure id Lorem quis cupidatat proident commodo dolore.

                    Commodo in dolor Lorem eiusmod commodo esse tempor amet. Magna dolore reprehenderit culpa quis ut officia excepteur officia ad. Anim tempor Lorem dolore eu excepteur deserunt.

                    Culpa enim adipisicing labore magna labore ex ipsum pariatur incididunt reprehenderit laborum elit enim mollit. Duis aute do ex laborum amet qui quis adipisicing nostrud dolore mollit non irure adipisicing. Ipsum eu nisi quis proident velit consectetur. Culpa id sunt in occaecat voluptate do velit ea ea est. Labore do ullamco quis eiusmod eiusmod. Elit nostrud reprehenderit ut amet mollit ex laboris do occaecat anim nisi.

                    Nisi velit labore quis ex reprehenderit sint id enim dolore cupidatat proident proident adipisicing magna. Mollit id sint id culpa culpa ut incididunt dolore officia ut ex fugiat. Cillum aute minim incididunt duis labore id esse elit laborum et laboris. Commodo commodo tempor anim esse adipisicing tempor officia incididunt officia mollit consequat. Laborum aliquip nisi ut incididunt magna sint elit laborum ea amet cupidatat Lorem enim occaecat.

                    Eiusmod veniam exercitation fugiat nostrud quis adipisicing occaecat consectetur ea Lorem. Cupidatat nulla labore duis voluptate eu voluptate. Consequat velit consectetur incididunt mollit Lorem nulla commodo fugiat excepteur reprehenderit ipsum. Cupidatat labore ea tempor pariatur enim cillum pariatur mollit cillum et ex excepteur irure. Deserunt deserunt aliquip enim qui amet magna do. Excepteur proident sit ipsum dolore nulla laboris.s
                </p>
            </section>
        </Zoom>
        <Bounce>
            <section>
                <h3>Ejemplo de titulo</h3>
                <p>
                    Nisi commodo culpa esse cupidatat non tempor in sit reprehenderit. Nostrud amet laborum quis esse labore ad reprehenderit ut irure. Velit enim in eiusmod sint ipsum dolore.

                    Nisi eiusmod est officia amet. Ad minim exercitation enim aliqua incididunt consequat qui ullamco. Nostrud labore irure pariatur ea do cupidatat aliqua.

                    Esse in ullamco ullamco commodo nisi. Ad mollit eiusmod duis aute enim duis. Nostrud fugiat laboris amet ex sit ut magna mollit ullamco minim nulla. Esse esse dolore eu excepteur dolore eu est incididunt est ullamco adipisicing id sunt sint. Elit non esse tempor minim nulla nulla esse est fugiat nulla labore ipsum culpa. Ullamco est anim aliqua quis esse velit laboris culpa. Excepteur irure sunt cupidatat commodo consectetur fugiat sit.

                    Nulla consequat pariatur irure in. Nisi proident aute excepteur in proident ipsum. Cillum eu duis anim veniam anim occaecat deserunt magna velit minim ex velit cillum ipsum. Lorem laborum minim cillum excepteur anim. Fugiat ullamco consectetur dolore elit nulla sunt consequat dolore veniam est incididunt. Nostrud anim non nulla cupidatat eu sint. Duis proident magna fugiat sint excepteur minim.
                </p>
            </section>
        </Bounce>
        
        <Rotate>
            <section>
                <h3>Ejemplo de titulo</h3>
                <p>
                    Dolore sint ex qui duis in officia irure ipsum nostrud cupidatat. Esse labore mollit cupidatat quis ipsum reprehenderit cupidatat enim sunt minim. Et adipisicing nisi aute pariatur. Veniam ad magna enim id. Duis sunt exercitation commodo officia nostrud commodo amet in eu sint excepteur. Labore ad Lorem nisi consequat proident eu aliquip ullamco mollit fugiat sint.

                    Tempor sint eu proident ipsum irure Lorem cillum occaecat eiusmod reprehenderit dolore tempor id. Deserunt do aute dolor sint ipsum sunt veniam non nostrud. Sint sit mollit et aute labore enim. Minim eu dolore id cupidatat minim laboris elit aute nisi aliqua nisi minim laborum adipisicing. Ex officia adipisicing laboris consectetur aute laboris sit cillum irure in in anim.
                </p>
            </section>
        </Rotate>
        </div>
    );
}

export default CReveal;