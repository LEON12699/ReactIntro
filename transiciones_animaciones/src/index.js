/*
 * @Author: Your name
 * @Date:   2020-04-03 00:18:26
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-07 00:25:44
 */
import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import TransicionesLinea from './Componentes/TransicionesLinea';
import Animaciones from './Componentes/Animaciones';
import Acordeon from './Componentes/acordeon';
import Slides from './Componentes/Slides';
import ReactTransiciones from './Componentes/ReactTransiciones';
import Carrusel from './Componentes/Carrusel';
import CReveal from './Componentes/CReveal';
import Ejemplo1 from './styleComponents/ejemplo';
import Anidados from './styleComponents/Anidados';
import Ejemplo2 from './styleComponents/ejemplo2';
import Atributos from './styleComponents/Atributos';
import Annimate from './styleComponents/Animate';

/*ReactDOM.render(
  <React.StrictMode>
    <TransicionesLinea></TransicionesLinea>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <Animaciones></Animaciones>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <Acordeon/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <Slides/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <ReactTransiciones/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <Carrusel/>
  </React.StrictMode>,
  document.getElementById('root')
);
ReactDOM.render(
  <React.StrictMode>
    <CReveal/>
  </React.StrictMode>,
  document.getElementById('root')
);
ReactDOM.render(
  <React.StrictMode>
    <Ejemplo1/>
  </React.StrictMode>,
  document.getElementById('root'));


ReactDOM.render(
  <React.StrictMode>
    <Anidados/>
  </React.StrictMode>,
  document.getElementById('root'));

  ReactDOM.render(
    <React.StrictMode>
      <Atributos/>
    </React.StrictMode>,
    document.getElementById('root'));

    */
  
   ReactDOM.render(
    <React.StrictMode>
      <Annimate/>
    </React.StrictMode>,
    document.getElementById('root'));

  // If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
