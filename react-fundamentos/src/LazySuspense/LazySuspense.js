/*
 * @Author: Your name
 * @Date:   2020-04-02 22:03:57
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-02 23:17:28
 */
import React, { Component, useState ,Suspense} from 'react';
//import Image from './components/image'
import { css } from "@emotion/core";
import  MoonLoader  from 'react-spinners/MoonLoader'
// Code Splitting 
// se pone en un archivo separado solo se renderiza cunado se lo usara
// gracias a la importacion dinamica
const Image =React.lazy(()=>import("./components/image")) // caracteristica experimental no

const App = () => {
    const [show, setShow] = useState(false);

    const toogle=()=>setShow(!show)
    const override = css`
    display: block;
    margin: 0 auto;
    border-color: red;
      `;

    const styleCenter={
        display:'flex',
        alignItems:'center',
        justifyContent:'center',
        flexDirection:'column'

    }

    return ( 
       
        <div style={styleCenter}>
         <button onClick={toogle}>
            {show?'Ocultar':'Mostrar'}
        </button>
            {show && (
                <Suspense fallback={<div><MoonLoader css={override} color='blue'/></div>}>
                    <Image/>
                </Suspense> 
            )}
        </div>
     );
}
 
export default App;