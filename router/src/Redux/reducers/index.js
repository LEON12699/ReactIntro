/*
 * @Author: Your name
 * @Date:   2020-04-08 22:20:00
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 01:27:11
 */
import {combineReducers} from 'redux'
import counter from './counter'
import user from './user';
import fruits from './FrutasReducer'
import todos from './Todo'
import posts from './PosterReducer'

export default combineReducers({
    counter,
    user,
    fruits,
    todos,
    posts
    
})