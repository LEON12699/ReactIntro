/*
 * @Author: Your name
 * @Date:   2020-03-26 23:14:02
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 23:33:01
 */
import React, { Component } from 'react';
class Entrada extends Component {
  /* constructor(){
       super()
       this.entrada = React.createRef()
   }*/
   entrada=React.createRef()

   componentDidMount(){
       this.focus()
   }

    focus=()=>{
        this.entrada.current.focus()
    }

   blur=()=>{
       this.entrada.current.blur()
   }

    render() { 
        return (
            <div>
                <input type="text" ref={this.entrada}/>
                <button onClick={this.focus}>
                    Focus
                </button>
                <button onClick={this.blur}>
                    blur
                </button>
            </div>
          );
    }
}
 

class Referencias extends Component {
    
    render() { 
        return ( 
            <div>
                <h1>React Refs 🌈</h1>
                <Entrada/>
            </div>
         );
    }
}
 
export default Referencias;