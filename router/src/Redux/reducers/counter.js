/*
 * @Author: Your name
 * @Date:   2020-04-08 22:17:54
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 22:49:16
 */
//Reducer
//funcion pura que regresa estado actual
import {INCREMENT,DECREMENT} from "../actions/Counter"
const inicial=0


function counter(state=inicial,action){
    if(action.type==INCREMENT){
        return state+1
    }
    if(action.type==DECREMENT){
        return state-1
    }
    return state
}

export default counter;