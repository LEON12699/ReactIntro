
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 22:01:23
 */
import React, { useState,useEffect,useLayoutEffect } from 'react';
import HeaderCF from './Header_cf';


const HookuseLayoutEffect=()=>{
    // retorna [value,function()]
    
    const [clicks,setCLicks]=useState(0)
    
    useEffect(()=>{
        console.log('useEffect 1')
    },[clicks])

    useEffect(()=>{
        console.log('useEffect 2')
    },[clicks])

    useLayoutEffect(()=>{
        console.log('useLayoutEffect 1')
    },[clicks])

    // estos se disparan antes que useEffect
    // use LayoutEffect es => sincrono
    // useEffect =>asincrono 
    // useEffect =>asincrono luego de la actualiacion
    // uselayourt se ejecuta antes de la actualizacion del dom  
    
    useLayoutEffect(()=>{
        console.log('useLayoutEffect 2')
    },[clicks])
    return ( 
        <div>
            <HeaderCF 
            title="Hook Use LyoutEffect() "/>
            <button onClick={()=>{
                setCLicks(clicks+1)
            }}>
                CLICKS ({clicks})
            </button>
        </div>
     );
}

export default HookuseLayoutEffect
;