/*
 * @Author: Your name
 * @Date:   2020-04-06 17:38:35
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-06 23:05:58
 */
import React,{useEffect,useState} from 'react';
import styled ,{ createGlobalStyle } from 'styled-components'

const GlobalStyle= createGlobalStyle`
    
    body{
        @import url('https://fonts.googleapis.com/css2?family=Lobster&display=swap');
        font-family: 'Lobster', cursive;
        
    }

`

const Header = styled.header`
    background: linear-gradient(35deg, black , yellow  );
    text-align:center;
    border-radius:0.2em;
    color:white;
    padding:0.3em;
    margin:0.3em;
    font-size:34px;
    `

const Button = styled.button`
    padding:0.6em 1em;
    background:${(props) => props.bg || 'black'};
    border-radius:0.2em;
    color:white;
    border:0;
    margin:0.4em;
    `



const BotonEspecial = styled(Button)`
    color:gray;
    transition:all 350ms ease-out;

    &:hover{
        transform:scale(1.3)
    }
    `


    

const Move = ({className}) => {
        const [mouseX, setMouseX] = useState(0);

        useEffect(()=>{
            window.addEventListener('mousemove',handle)
            return ()=>{
            window.removeEventListener('mousemove',handle)
            }
        },[])

        const handle=(e)=>{
            setMouseX(e.clientX)
        }

        return ( 
            <div className={className}>
                {mouseX}
            </div>
         );
    }
     
    const MoveStyled=styled(Move)`
    background:blue;
    font-size:30px;
`

const Ejemplo2 = () => {
    return (
        <div>
        <GlobalStyle/>
        <h2>Ver variable globales</h2>
            <Header>
                <h1>Style Componente</h1>
            </Header>
            <Button>
                Boton 
            </Button>
            <BotonEspecial>
                nuevo boton
            </BotonEspecial>
            <MoveStyled>
            </MoveStyled>
        </div>
    );
}

export default Ejemplo2;