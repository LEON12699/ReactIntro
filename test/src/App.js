/*
 * @Author: Your name
 * @Date:   2020-04-09 22:43:15
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:33:20
 */
import React from 'react';
import COunter from './Componentes/Counter'

export const Title = () => {
      return ( 
        <h1>Introduccion a UNIT TESTING</h1>
       );
}
 

function App() {
  return (
    <section>
    <Title/>
      <div className="container">
        <span num={3} active={false}></span>
        <span num='3' active={false}></span>
      </div>
      <input type="text" />
      <p>parrafo</p>
      <div>
        <p>Parrafo1</p>
        <p>parrafo2</p>
      </div>
      <COunter/>
    </section>
  );
}

export default App;
