/*
 * @Author: Your name
 * @Date:   2020-03-26 00:01:27
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 21:58:03
 */
import React, { Component } from 'react';
import FakerData from "../user";
//import "./css.css"
import Css from "./css.module.css"
/*const imagenes=[
    {
        author:{
            name:`Vlado Bagacian`,
            avatar:`https://avatarfiles.alphacoders.com/226/226494.jpg`

        },
        source:``,
        views:153,
        id:1
    },
    {
        author:{
            name:`Vlado Bagacian`,
            avatar:`https://avatarfiles.alphacoders.com/226/226494.jpg`

        },
        source:`https://i.imgur.com/1Q6zOt3.jpeg`,
        views:153,
        id:2
    }
]
*/

const imagenes = FakerData.crear_usuarios(10)

const Imagen=({image})=>(
    <div className={Css.card}>
        <img
        src={image.source}
        alt="imagen"
        className={Css.image}>

        </img>
        <div className={Css.footer}>
            <img
            src={image.author.avatar}
            alt="Avatar"
            className={Css.avatar}>
                
            </img>
            <div>
                {image.author.name}
            </div>
            <div>
                {image.views}
            </div>
        </div>
    </div>
)

class IterComponentes extends Component {
    
    render() { 
        return ( 
            <div className={Css.imagens}>
                {imagenes.map(image=>(
                    <Imagen
                    key={image.id} 
                    image={image}/>
                )  )}
            </div>
         );
    }
}
 
export default IterComponentes;