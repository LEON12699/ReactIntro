/*
 * @Author: Your name
 * @Date:   2020-04-10 02:32:36
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:36:26
 */
import React from 'react';
import { useState } from 'react';

const COunter = () => {

    const [clicks, setClicks] = useState(0);        
    const add =()=> setClicks(clicks+1)
    const menos=()=>setClicks(clicks-1)

    return ( 
        <div>
            <button onClick={add}>
                Incrementar
            </button>
            <button onClick={menos}>
                restar
            </button>
            <h1>{clicks}</h1>
        </div>
     );
}
 
export default COunter;