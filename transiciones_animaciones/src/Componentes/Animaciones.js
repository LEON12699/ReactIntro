/*
 * @Author: Your name
 * @Date:   2020-04-04 17:41:29
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-04 18:40:54
 */
import React, { useState } from 'react';
import Header from './Header';
import "./animaciones.css"

const Animaciones = () => {
    const [active, setActive] = useState(false);

    const toogle = () => setActive(!active)

    const clases=active?'header header-active':'header'
    return (
        <div>
            <button
                onClick={toogle}>
                {active ? 'Desactivar' : 'Activar'}
            </button>
            <Header title="ANIMACIONES CSS" show={active} className={clases} />
        </div>
    );
}

export default Animaciones;