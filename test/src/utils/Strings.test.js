/*
 * @Author: Your name
 * @Date:   2020-04-09 23:04:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:48:40
 */
import { saluda,getId,despide } from './Strings'

describe('Pruebas strings', () => {

    const saludo = saluda('Israel');
    const Id = getId();
    const despedir = despide()
    test('Probando funcion saluda', () => {
        expect(saludo).toMatch("Hola soy"); 
        /*to match evalua si existe el texto en el string*/
    });
    
    test('Probando getId', () => {
        expect(Id).toMatch(/\d{2}-\d{3}/); 
        
    });

    test('probando despedir', () => {
        expect(despedir).not.toMatch('Byes')
    });
    
});