/*
 * @Author: Your name
 * @Date:   2020-04-08 23:22:35
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:41:50
 */
import {ADD_FRUIT} from '../actions/FruitActions'

const inicial=[
    'FRESA',
    'MANZANA'
]

function fruits(state=inicial,action){
    switch (action.type) {
        case ADD_FRUIT:
            return state.concat(action.payload.fruit)
        default:
            return state;
    }
}

export default fruits