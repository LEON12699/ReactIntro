/*
 * @Author: Your name
 * @Date:   2020-04-08 22:47:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 22:50:36
 */
export const INCREMENT ='INCREMENT'
export const DECREMENT ='DECREMENT'

export const increment=()=>{
    return{
        type:INCREMENT
    }
}
export const decrement=()=>{
    return{
        type:DECREMENT
    }
}