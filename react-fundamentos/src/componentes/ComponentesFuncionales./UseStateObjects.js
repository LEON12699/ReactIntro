/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 17:12:50
 */
import React, { useState } from "react";
import HeaderCF from "./Header_cf";

const UseStateObjects = () => {
    // retorna [value,function()]

    const [state, setState] = useState({ clicks: 0 })

    const mezcladorEstado =(nextState)=>{
        setState({
            ...state,
            ...nextState
        })
    }
    
    const addClicks = () => {
        mezcladorEstado({
            clicks:state.clicks+1
        })
    };

    const handleInput = e => {
        const title = e.target.value;
        setState({ ...state,title });
    };

    return (
        <div>
            <HeaderCF 
            title="Hook UseState -Objetos" 
            subtitle={state.title}>

            </HeaderCF>
            <input
                type="text"
                onChange={handleInput}
                value={state.title}>
            </input>
            <button onClick={addClicks}>CLicks ({state.clicks})</button>
        </div>
    );
};

export default UseStateObjects;
