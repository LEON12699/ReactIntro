/*
 * @Author: Your name
 * @Date:   2020-03-24 16:52:23
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 17:03:52
 */
import React, { Component } from "react";

const styles ={
    height:'200px',
    background:'#DEDEDE',
    padding:'1em',
    boxSizing:'border-box'
}
class Mouse extends Component{
    state ={
        x:0,
        y:0
    }
    manejador=(event)=>{
    this.setState(   {
        x:event.clientX,
        y:event.clientY
    })
    }

    render(){
        return(
        <div style={styles}
        onMouseMove={this.manejador}>
        <div>
            x: {this.state.x}
        <hr></hr>
            y: {this.state.y}
        </div>
        </div>)
    }
}

export default Mouse