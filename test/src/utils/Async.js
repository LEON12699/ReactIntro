/*
 * @Author: Your name
 * @Date:   2020-04-10 00:53:19
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 01:46:28
 */

 import {setTimeout} from 'timers'

export const getData=(callback)=>{    
    const name ="israel leon"
    setTimeout(()=>{
        callback(name)
    },200)
}


export const getDataPromise =()=>{
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            resolve('israel leon')
        },200)
    })
}

export const getDataPromiseError =()=>{
    return new Promise((resolve,reject)=>{
        setTimeout(()=>{
            reject('error')
        },200)
    })
}

export const getUsers = async () => {
    const res = await fetch('https://jsonplaceholder.typicode.com/users')
    const user = await res.json()
    return user
}
 

getData(name=>{
    console.log(name)
})
