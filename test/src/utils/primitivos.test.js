/*
 * @Author: Your name
 * @Date:   2020-04-10 00:17:36
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:22:12
 */
const getValue = (exp) => {
    switch (exp) {
        case 'definido':
            return {};
        case 'indefinido':
            return undefined
        case 'verdadero':
            return true;
        case 'falso':
            return false;
        default:
            return null
    }
}


describe('pruebas de valores nulos undefines y boleanos', () => {
    test('Validar si existe o esta definido un valor', () => {
        
        expect(getValue("definido")).toBeDefined()
        //expect(getValue("indefinido")).toBeDefined()

    });

    test('Validar si  un valor es nulo', () => {
        
        expect(getValue()).toBeNull()
        //expect(getValue("indefinido")).toBeDefined()
        
    });

    test('Validar si  un valor es verdadero', () => {
        
        expect(getValue("verdadero")).toBeTruthy()
        //expect(getValue("indefinido")).toBeDefined()
        
    });

    test('Validar si  un valor es false', () => {
        
        expect(getValue("falso")).toBeFalsy()
        //expect(getValue("indefinido")).toBeDefined()
        
    });

    test('Validar si  un valor es Nan', () => {
        
        expect(NaN).toBeNaN()
        //expect(getValue("indefinido")).toBeDefined()
        
    });

});