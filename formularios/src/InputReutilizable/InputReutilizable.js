/*
 * @Author: Your name
 * @Date:   2020-03-27 17:14:50
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 17:29:49
 */
import React, { Component } from 'react';
class InputsC extends Component {
    state = { 
        text:"",
        error:false,
        color:`#E8E8E8`
     }
     
     actualizar=(event)=>{
        const text=event.target.value
        //const error = (text!=='' && text.length<5) || text==''
        let color =`green`
        if(text.trim()===``){
            color=`E8E8E8`
        }
        if(text.trim()!==`` && text.trim().length<5){
            color=`red`
        }
        this.setState({ text,color});

        // Propagando datos al padre
        this.props.onChange(this.props.name,text)


    }

    render({text}=this.state) { 
        const styles={
            border: `1px solid ${this.state.color}`/*this.state.error?`1px solid red`:`1px solid #E8E8E8`*/,
            padding:`0.3em 0.6em`,
            outline:`none`
        }
        return ( 
            <input type="text"
            value={this.state.text}
            onChange={this.actualizar}
            style={styles}
            placeholder={this.props.placeholder}></input>
         );
    }
}
 




class InputReutilizable extends Component {
    
    state = {
        name:``,
        email:``
      }
    
    actualizar=(name,text)=>{
        this.setState({
            [name]: text 
        });
    }

    render() { 
        return ( 
            <div>
                <h1>Inputs Controlados2 💻</h1>
                <InputsC
                onChange={this.actualizar}
                placeholder="NOmbre completo"
                name='name'></InputsC>
                <InputsC
                onChange={this.actualizar}
                placeholder="Email"
                name='email'
                />
                <h2>Nombre : {this.state.name}</h2>
                <h2>Email : {this.state.email}</h2>
            </div>
        );
}
}
 
export default InputReutilizable;