/*
 * @Author: Your name
 * @Date:   2020-04-09 23:51:26
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 23:58:55
 */
import  { CreateStore} from './Arrays'

describe('Pruebas a arrays/listas', () => {
    test('probando agregar frutas', () => {
        const store = CreateStore()
        store.addFruit("Fresa")

        expect(store.getFruits()).toStrictEqual(["Fresa"])

    });

    test('probando Agregar dos frutas', () => {
        const store = CreateStore()
        store.addFruit("Fresa")
        store.addFruit('Mango')
        expect(store.getFruits()).toStrictEqual(["Fresa","Mango"])

    });


    test('probando si contiene frutas especifica', () => {
        const store = CreateStore()
        store.addFruit("Fresa")
        store.addFruit('Mango')
        expect(store.getFruits()).not.toContain("Fres")
        expect(store.getFruits()).toContain("Fresa")

    });

    test('probando longitud del array', () => {
        const store = CreateStore()
        store.addFruit("Fresa")
        store.addFruit('Mango')
        expect(store.getFruits()).toHaveLength(2)

    });

    test('probando un objeto con la informacion de fruta', () => {
        const store = CreateStore()
        store.addFruit({name:"Fresa",Price:20})
        store.addFruit({name:"Mango",Price:22})
        
        expect(store.getFruits()).toContainEqual({name:"Fresa",Price:20})

    });

});