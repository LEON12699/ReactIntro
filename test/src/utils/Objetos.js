/*
 * @Author: Your name
 * @Date:   2020-04-09 23:48:56
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 00:08:00
 */


export const CreateStore = () => {
    let store = []
    return {
        additem:(item)=>{
            store=[...store,item]
        },

        getStore:()=>store,

        getByid:(id)=>store.filter(item=>item.id===id)[0]
    };
}
 