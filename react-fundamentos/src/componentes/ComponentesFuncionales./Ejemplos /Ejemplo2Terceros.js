
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 22:48:16
 */
import React, { useState,useEffect,useRef } from 'react';
import HeaderCF from './../Header_cf';
import { useDebounce } from "use-debounce"


const Ejemplo2Terceros = () => {
    // retorna [value,function()]
    
    
    const [ products, setProducts ] = useState([])
    const [name,setName]=useState('')
    const [search]=useDebounce(name,1000)

    const handleInput=(e)=>{
        setName(e.target.value)
    }   

    useEffect(()=>{
        
            fetch('https://universidad-react-api-test.luxfenix.now.sh/products?name='+name)
            .then(res => res.json())
            .then(data => setProducts(data.products))
        
    },[search])

    return (

        <div>
            <HeaderCF
                title="Hook useRef Ejemplos2 terceros"
            />
            <input
            type="text"
            onChange={handleInput}
            />
            <ul>
                {products.map(product=>(
                    <li key={product.id}>
                        {product.name}
                    </li>
                ))}
            </ul>
            
        

        </div>

    );
}

export default Ejemplo2Terceros
    ;