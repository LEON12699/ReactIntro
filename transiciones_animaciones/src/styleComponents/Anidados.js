/*
 * @Author: Your name
 * @Date:   2020-04-06 17:07:00
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-06 17:37:06
 */
import React,{useState} from 'react';
import styled from 'styled-components'

const Header = styled.header`
    background: linear-gradient(45deg, black , yellow  );
    text-align:center;
    border-radius:0.2em;
    color:white;
    padding:0.3em;
    margin:0.3em;
    font-size:34px;
    transition:opacity 400ms ease-out;
    h1{
        color:lightblue
    }

    div{
        width:50%;
        height:50%;
        background-attachment: fixed;
        background:radial-gradient(ellipse at center, black 0%, yellow 100%)
    }

    .big{
        color:blueviolet;
        width:100%;
        text-align:right;
        
    }

    &:hover{
        opacity:0.3;
        background:transparent;
        border:2px solid black;
        div{
            color:blueviolet;
            }
    }
    `

    const Button = styled.button`
        padding:0.6em 1em;
        background:${(props)=>props.bg||props.active?'black':'blue'};
        border-radius:0.2em;
        color:white;
        border:0;
        margin:0.4em;
        transition:all 350ms ease-out;
        &:hover{
            opacity:0.6;
        }
    `



const Anidados = () => {

    const [active, setActive] = useState(false);

    const handle=()=>setActive(!active)

    return ( 
        <div>
        <Header>
            <h1>Style componente</h1>
            <div>*******</div>
            <div className='big'>
                Clasname big
            </div>
        </Header>
        <Button bg='linear-gradient(to right, black 0%, white 100%)'>
            Boton
        </Button>
        <Button onClick={handle} active={active}>
            Boton
        </Button>
        </div>
     );
}
 
export default Anidados;