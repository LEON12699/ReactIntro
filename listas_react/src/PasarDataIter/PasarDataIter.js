/*
 * @Author: Your name
 * @Date:   2020-03-26 22:00:15
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 23:10:57
 */
import React, { Component } from 'react';
import FakerD from "../user";



class PasarDataIter extends Component {
    state = { 
        frutas: FakerD.crear_frutas(100),
        frutaSeleccionada:{}
     }

     select=(frutaSeleccionada,event)=>{
        this.setState({ frutaSeleccionada });
     }

     deseleccionar=(frutaSeleccionada,event)=>{
         this.setState({ frutaSeleccionada  });
     }

    render() {
        const {frutas,frutaSeleccionada}=this.state 

        return ( 
        <ul>
            {frutas.map((fruit,index)=>(
                <li key={fruit.id}
                onMouseEnter={this.select.bind(this,fruit)}
                onMouseLeave={this.deseleccionar.bind(this,{})}
                style={{
                    color:frutaSeleccionada.id===fruit.id?`blue`:`#000`}}>
                    {index+1}.- {fruit.name} -- $ {fruit.price}
                </li>
            ))}
        </ul>
        );
    }
}
 
export default PasarDataIter;