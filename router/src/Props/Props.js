/*
 * @Author: Your name
 * @Date:   2020-04-07 17:30:37
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-07 22:50:39
 */
import React from 'react';
import {BrowserRouter,Route,Link,NavLink} from 'react-router-dom'

//link sin que se recargue la pagina

const Hola = () => {
    return ( 
        <h1>HOLA</h1>
     );
}

const Product = () => {
    return ( 
        <h2>Productos</h2>
     );
}

const Home = (props) => {
    console.log(props)
    return ( 
        <h2>Home</h2>
     );
}
 
const Holaninja = () => {
    return ( 
        <h1>Hola ninja</h1>
     );
}

const navStyles= {
    display:'flex',
    justifyContent:'space-around'
}

/** replace =>remplaza el historial con la ruta anterior*/
const Navigation = () => {
    return ( 
        <nav style={navStyles}>
            <Link to={{
                pathname:'/',
                search:'?ordenar=nombre',
                hash:'#hash',
                state:{
                    name:'Ninja PRO',
                    age:25
                }}} >home </Link>
            <Link to="/hola">HOla </Link>
            <Link to="/products" replace >Productos </Link>
        </nav>
     );
}

const navActive={
    color:'orangered'
}

//activeClassName es para agregar nombre de clases
const Navigation2=()=>(
    <nav style={navStyles}>
    <NavLink to='/' exact activeStyle={navActive} >home </NavLink>
    <NavLink to="/hola" activeStyle={navActive}>HOla </NavLink>
    <NavLink 
    to="/products" 
    activeStyle={navActive} 
    isActive={(match,location)=>{
        if(!match)return false
        return !match.isExact
    }}>Productos </NavLink>
</nav>
)
    // exact para que solo cunado sea /hola y no tambine /hola/ex..
 // strict se puede añadir para que sea estricto
// sensitive sensible a mayusculas y minisculas
// Componente se puede pasar un arrow function con marcado 
// cada vez de nuevo renderizados se regeneran 
// con render no se crea nuevas funciones

const Props = () => {
    return ( 
        <BrowserRouter>
        <Navigation2/>
        <div align="center">Practice de routes </div>
            <Route path='/' exact component={Home}/>
            <Route path='/hola' exact render={Hola}/>
            <Route path='/hola/Ninja' sensitive  component={Holaninja}/>
            
            <div>
                <Route path='/products/:id?'>
                    {(props)=>{
                        const {match}=props

                        if(!match) return null
                        console.log(props)
                        console.log('siempre se renderiza')

                        return(
                            <Product/>
                        )
                    }}
                </Route>
            </div>
        </BrowserRouter>
     );
}
 
export default Props;