/*
 * @Author: Your name
 * @Date:   2020-03-25 23:56:50
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 00:00:08
 */
import React, { Component } from 'react';

const users=[
    {id :1,name:`Juanito perez`,country:`Chile`},
    {id :2,name:`Israel Leon`,country:`Ecuador`},
    {id :3,name:`Stefania Peña`,country:`Colombia`},
    {id :4,name:`Jose Luis`,country:`Mexico`},
    {id :5,name:`Padro pedro`,country:`USA`},

]

class PropKey extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
                <ul>
                    {users.map((user,index)=>(
                        <li key={user.id}>
                            {user.name}
                        </li>
                    ))}
                </ul>
            </div>
         );
    }
}
 
export default PropKey;