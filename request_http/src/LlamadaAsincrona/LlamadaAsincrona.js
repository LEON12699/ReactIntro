/*
 * @Author: Your name
 * @Date:   2020-03-27 23:44:22
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 00:20:44
 */
import React, { Component } from 'react';
class LlamadaAsincrona extends Component {
    state = { 
        movie:{},
        cargando:false
     }

     handleSubmit=(event)=>{
        this.setState({ cargando: true });
        event.preventDefault()
        const title=event.target[0].value
        
        const url=`http://www.omdbapi.com/?i=tt3896198&apikey=829c16ef`
        fetch(url+`&t=`+title)
        .then(res=>res.json())
        .then(movie=>this.setState({ movie,cargando:false}))
        .catch(error=>console.log(error))
    }

    render() { 

        const {movie,cargando}=this.state
        return ( 
            <div>
                <h1>
                    Ejemplo HTTP buscador de Peliculas
                </h1>
                <form onSubmit={this.handleSubmit}>
                    <input
                    type="text"
                    placeholder="Nombre de pelicula">

                    </input>
                    🎫
                    <button>
                        Buscar
                    </button>
                </form>
                {cargando &&  
                <h2>Buscando espere ...</h2> }
                { Object.keys(movie).length !==0 
                 && !cargando &&
                <div>
                    <h1>{movie.Title}</h1>
                    <p>{movie.Plot}</p>
                    <img src={movie.Poster} alt="poster"></img>
                </div>}
            </div>

         );
    }
}
 
export default LlamadaAsincrona;