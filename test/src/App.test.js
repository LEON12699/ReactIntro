/*
 * @Author: Your name
 * @Date:   2020-04-10 01:38:05
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-10 02:02:26
 */

 /* shallow test a componentes como una sola unidad cada uno tiene su contexto
 
 mount para interactuar con apis del dom o componentes envueltos
 lo mas parecido a un entorno de navegador el contexto es global

 render transforma el arbol en un marcado html y lo envuelve en cheerio 
 similar a jquery para interacciones

 **/
import React from 'react';
import Enzyme,{shallow,mount,render} from 'enzyme';
import Adapter from 'enzyme-adapter-react-16';
import App ,{ Title }from './App'

Enzyme.configure({ adapter: new Adapter() });

describe('probando componente App', () => {
        test('probando renderizado', () => {
            const wraper = shallow(<App/>)
            // por css
//            console.log(wraper.find(".container").html())
            //por id
            //console.log(wraper.find("#main").html())
            //avanzados
            //console.log(wraper.find("div > p").html())
            //console.log(wraper.find("div + p").html())
            //console.log(wraper.find("div ~ p").html())
            //console.log(wraper.find("[num=3]").html())
            //console.log(wraper.find(Title).html())
           

            // expect(wraper.find('h1').html()).toBe('<h1>Introduccion a UNIT TESTING</h1>')
           // expect(wraper.find('h1')).toHaveLength(1)
            
        });
        
});