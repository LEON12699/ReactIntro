/*
 * @Author: Your name
 * @Date:   2020-03-26 17:20:33
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-26 23:42:32
 */
import Faker from "faker"
Faker.locale="es"

class FakerData {

static crear_usuarios(tamaño){
    var users=[]
    for (let i = 0; i < tamaño; i++) {
        const user={
            id:i+1,
            email:Faker.internet.email(),
            direccion:Faker.address.streetName(),
            author:{
                name:Faker.name.firstName(),
                avatar:Faker.internet.avatar(),
            },
            views:Faker.random.number(),
            source:Faker.image.image()
        }
        users.push(user)
        
    }
    return users
}

static crear_frutas(tamaño){

    console.log(Faker.locale)
    var frutas=[]
    for (let i = 0; i < tamaño; i++) {
        const user={
            id:i+1,
            price:Faker.lorem.word(),
            name:Faker.commerce.product()
                }
        frutas.push(user)
        
    }
    return frutas
}
}

export default FakerData
