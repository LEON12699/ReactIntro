/*
 * @Author: Your name
 * @Date:   2020-03-27 22:46:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 23:04:09
 */
import React, { Component } from 'react';
class LlamadaApi extends Component {
    
    state = { 
        users:[],
        cargando:true
        
    }
    componentDidMount(){

        fetch('https://jsonplaceholder.typicode.com/users')
        .then((res)=>res.json())
        .then(users=>this.setState({ users,cargando:false  }))
        .catch(error=>console.log(error.toString()))/*setTimeout(()=>{
            this.setState({ 
                text: 'HOla react' });
            
        },1000)

        setTimeout(()=>{
            this.setState({ 
                text: 'EL poder de react' });
            
        },2500)*/


    }
    render() {
        if (this.state.cargando){
            return <h1>Cargando...</h1>
        } 
        return (
            <div>
                <h1>Peticion HTTP</h1>
                <h2>{this.state.text}</h2>
                <ul>
                    {this.state.users.map(user=>(
                        <li key={user.id}>
                            {user.name}
                            <a href={`http://${user.website}`}>
                                Website
                            </a>
                        </li>
                    ))}
                </ul>
            </div>
         );
    }
}
 
export default LlamadaApi;