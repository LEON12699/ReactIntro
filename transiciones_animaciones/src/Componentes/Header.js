/*
 * @Author: Your name
 * @Date:   2020-03-31 16:41:00
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-04 18:39:35
 */
import React from 'react';

const Header = (props) => {
    const subtitleStyle = {
        fontWeight: "bold"
    };

    const {show}=props

    const activeStyles={
        background: `radial-gradient(circle, rgba(217,15,23,1) 37%, rgba(17,19,20,1) 96%)`,
        transform:`scale(1)`,
        color:'orange'
    }
    
    let headerStyle = {
        background:`#3d1f9a` ,
        transform:`scale(0.5)`,
        margin: "0.6em",
        position:"relative",
        color:"white",
        borderRadius: "0.8em",
        border: "1px solid #d2d2d2",
        padding: "2em 0.4em",
        fontFamily: "monospace",
        fontSize: "27px",
        textAlign: `center`,
        transition:`all 800ms ease-out`
    };

    if(show){
        headerStyle={
            ...headerStyle,
            ...activeStyles
        }
    }

    const {title,subtitle,className=''}=props
    return (
        
        <header style={className==''?headerStyle:{}} className={className} >
            <div>({title} )</div>
            <div style={subtitleStyle}>
            {subtitle}
        <span role="img" aria-label="flame">
                    🔥
        </span>
            </div>
        </header>
    );
};

export default Header