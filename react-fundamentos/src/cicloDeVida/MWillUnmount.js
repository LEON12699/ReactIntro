/*
 * @Author: Your name
 * @Date:   2020-03-29 22:15:08
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-29 22:32:11
 */
import React, { Component } from 'react';

class Time extends Component {
    state = { 
        time:0,
        isPlaying:true
     }

     tick=null
     play=()=>{
         this.setState({ isPlaying: true });
        this.tick=setInterval(()=>{
            this.setState(state=>({
                time:state.time+ 1
            }));

        },1000)
     }

     pause=()=>{
         this.setState({ isPlaying: false });
        clearInterval(this.tick)
     }

     componentWillUnmount(){
        this.setState({ isPlaying: false });
        clearInterval(this.tick)
         
     }

     toogle=()=>{
         if(this.state.isPlaying){
             this.pause()

         }else{
             this.play()
         }
     }

     componentDidMount(){
         this.play()
     }


    render() { 
        const{time,isPlaying}=this.state
        return (
            <div>
                
                <h2>{time}</h2>
                <button onClick={this.toogle}>
                   {isPlaying? "pause":"play"}
                </button>
            
            </div>
          );
    }
}


class MWillUnmount extends Component {
    state = { 
       mostrar:true
     }

     desmontar=()=>{
         this.setState({ mostrar: false });
     }

    render() { 
       
        return (
            <div>
                <h1>MWillUnmount</h1>
                <button onClick={this.desmontar}>
                    Desmontar
                </button>
              {this.state.mostrar && <Time/>} 
            </div>
          );
    }
}
 
export default MWillUnmount;