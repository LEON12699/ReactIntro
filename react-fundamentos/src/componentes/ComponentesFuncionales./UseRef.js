
/*
 * @Author: Your name
 * @Date:   2020-03-31 16:42:13
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 22:20:15
 */
import React, { useRef } from 'react';
import HeaderCF from './Header_cf';



const UseRef = () => {
    // retorna [value,function()]

const entrada = useRef()
const Focus=()=>entrada.current.focus()
const Blur=()=>entrada.current.blur()
    return (

        <div>
            <HeaderCF
                title="Hook useRef"
            />
            <input
                type="text"
                placeholder="Ingresa texto"
                ref={entrada}
            >
            </input>
            <button onClick={Focus}>
                FOCUS
            </button>
            <button onClick={Blur}>
                BLUR
            </button>

        </div>

    );
}

export default UseRef
    ;