/*
 * @Author: Your name
 * @Date:   2020-03-30 23:01:28
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-31 00:05:37
 */
import React, { Component } from "react";
import "./styles.css";
import Header from "../header";
import Proptypes from 'prop-types'
//{provider,consumer}

class Resize extends Component {
    static propTypes={
        render:Proptypes.func.isRequired
    }
    componentDidMount(){
        window.addEventListener('resize',
        this.handleresize)

        
    }

    componentWillUnmount(){
        window.removeEventListener('resize',this.handleresize)
    }
    handleresize=()=>{
        this.setState({ 
            width:window.innerWidth,
        height:window.innerHeight  });

    }
    state = {
        width:window.innerWidth,
        height:window.innerHeight
      }
    render() { 
        const {width,height}=this.state
      const {render} =this.props
        return render({width,height})
    }
}
 


class RenderProps2 extends Component {
  state = {
   
  };

  render() {
    return (
     
        <div className="headerStyle">
          <Header 
          title="(Hijo a padre)" 
          subtitle="Ejemplo Render Props" />
          <Resize render={({width,height})=>{
              return(
                  <div>
                    <h2>width:{width}</h2>
                    <li>height: {height}</li>
                  </div>
              )
          }}/>
        </div>

    );
  }
}

export default RenderProps2;
