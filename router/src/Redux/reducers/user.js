/*
 * @Author: Your name
 * @Date:   2020-04-08 22:18:18
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:13:03
 */

import {UPDATE_NAME} from '../actions/userActions'


const inicial={
    name:'Israel Leon',
    country:'Ecuador'
}
function user(state=inicial,action){
    switch (action.type) {
        case UPDATE_NAME:
         return {
            ...state,
            name:action.payload.name,
             
         }
        default:
            return state
    }
    
}

export default user;