/*
 * @Author: Your name
 * @Date:   2020-04-09 00:33:57
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-09 00:33:57
 */
import React from 'react';

const Formulario = ({onSubmit}) => {
    return ( 
        <form onSubmit={onSubmit}>
                <input type='text'/>
                <button>
                    Agregar
                </button>
            </form>
     );
}
 
export default Formulario;