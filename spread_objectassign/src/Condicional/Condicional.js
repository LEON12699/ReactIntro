/*
 * @Author: Your name
 * @Date:   2020-03-24 23:11:47
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 23:22:33
 */
import React from "react";

const Saludo =(props)=>{

    return(<div>
    <div>
        {props.name && <strong>{props.name}</strong>}
    </div>
        {props.saluda?
        (
            <h1>
                Hola, tu eres genial <span role="img" aria-label="heart">💚</span>
            </h1>
        ):
        (
            <h1>
                Wops no hay saludos hoy
            </h1>
        )}
    </div>
    )
  
}

const Condicional=()=>(
    <div>
        <Saludo saluda ></Saludo>
    </div>
)

export default Condicional