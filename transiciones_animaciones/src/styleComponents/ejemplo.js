/*
 * @Author: Your name
 * @Date:   2020-04-05 23:42:55
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-06 17:02:54
 */
import React from 'react';
import styled from 'styled-components'
//import './styles.css'
import Constantes from './Constantes'

//Variable BAsica
const PrimaryColor = '#EF5350'
// Variable Completa
const paddingBasic='padding:0.4em;'

// Variables dinamicas
const getLinearGradient=(rot,color1,color2)=>{
    return `background: linear-gradient(${rot}, ${color1} 30% ,black 60%, ${color2} 100%);`
}

/*background:${PrimaryColor/*var(--colorPrincipal)};*/

const Header = styled.header`

    ${getLinearGradient('180deg',`${PrimaryColor} `,'#00008a')}
    text-align:center;
    border-radius:0.2em;
    color:white;
    padding:0.3em;
    margin:0.3em;
    font-size:14px;
    `
    
const Subtitle= styled.h1`
    color:${Constantes.PrimaryColor};
    ${paddingBasic}
`

const Ejemplo1 = () => {
    return (
        <div>
            <Header>
                <h1>
                    Styled components
            </h1>
            </Header>
            <Subtitle>
                subtitulo 
            </Subtitle>
        </div>
    );
}

export default Ejemplo1;