/*
 * @Author: Your name
 * @Date:   2020-03-23 16:00:54
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 15:27:42
 */
import React from 'react'
//import './frutas.css'
import moduloEstilo from './frutas.module.css'

class Fruta extends React.Component{
    state={
        cantidad:0
    }
    
      // arrow()=>{} functions heredan el this del padre mas no las funciones
    
    limpiar=()=>this.setState({cantidad:0})
    
    agregar=()=>{
        this.setState({cantidad:this.state.cantidad+ 1})
    }
    
    restar=()=>{
        this.setState({cantidad:this.state.cantidad- 1})
    }
    
    render(){
       const tiene= this.state.cantidad>0
       /* const styles={
            border:`1px solid black`,
            marginBottom :`1em`,
            borderRadius:`0.5em`,
            padding:`1em`,
            background:this.state.cantidad>0? `linear-gradient(45deg,black,#4a02f7)`:`#FFF`,
            color:tiene?`#FFF`:`#000`
            ,transition:`all 500ms ease-out`
        }*/
        //const clases=tiene?'fruta-activa ':'fruta'
        const clases=`${moduloEstilo.card} ${tiene?moduloEstilo['card-active']:''}`
        return(
        <div /*style={styles}*/ className={clases}>
            <h1>{this.props.name} ${this.props.price}</h1>
            <div>Cantidad: {this.state.cantidad}</div>
            <button 
            onClick={this.restar}>
            -
            </button>
            <button 
            onClick={this.agregar}>
            +
            </button>
            <button 
            onClick={this.limpiar}>
            Limpiar
            </button>
            
            <p>Total ${this.props.price * this.state.cantidad}</p>
           

            <hr/>
          </div>
        )
      }
    }

export default Fruta