/*
 * @Author: Your name
 * @Date:   2020-03-27 22:24:50
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-28 00:47:13
 */
import React from 'react';
import ReactDOM from 'react-dom';
import * as serviceWorker from './serviceWorker';
import LlamadaApi from './LlamadaApi';
import LlamadaAsincrona from './LlamadaAsincrona/LlamadaAsincrona';
import LlamadaAxios from './LlamadaAxios';
import LlamadaAsync from './LlamadaAsync';

/*
ReactDOM.render(
  <React.StrictMode>
    <LlamadaApi/>
  </React.StrictMode>,
  document.getElementById('root')
);

ReactDOM.render(
  <React.StrictMode>
    <LlamadaAsincrona/>
  </React.StrictMode>,
  document.getElementById('root')
);
*/

ReactDOM.render(
  <React.StrictMode>
    <LlamadaAsync  />
  </React.StrictMode>,
  document.getElementById('root')
);


// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
