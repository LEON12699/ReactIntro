/*
 * @Author: Your name
 * @Date:   2020-03-27 21:53:32
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-27 22:12:52
 */
import React, { Component } from 'react';

class Boton extends Component {
    state = { 
        tieneError:false
     }

    disparar=()=>{
        this.setState({ tieneError: true });
    }

    render() { 
        if(this.state.tieneError){
            throw new Error('he fallado')
    }
        
        return (   
            <button onClick={this.disparar}>
                Boton bug
            </button>
              );
    }
}
 

class LimiteErrores extends Component {
    state={
        tieneError:false
    }

    componentDidCatch(error,errorInfo){
        this.setState({ tieneError: true,
        error });
    }
    
    render() { 
        if(this.state.tieneError){
            return(<div>
                Wops ha ocurrido un error comunicate con soporte tecnico
                <div style={{color:`orangered`}}>
                    {this.state.error && this.state.error.toString()}
                </div>
            </div>)
        }

        return ( this.props.children );
    }
}
 

class Errores extends Component {
    state = {  }
    render() { 
        return ( 
            <div>
            <LimiteErrores>
                <Boton></Boton>
                </LimiteErrores>
                <LimiteErrores>
                <Boton></Boton>
                </LimiteErrores>
                <LimiteErrores>
                <Boton></Boton>
                </LimiteErrores>
            </div>
         );
    }
}
 
export default Errores;