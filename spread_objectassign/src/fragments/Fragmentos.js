/*
 * @Author: Your name
 * @Date:   2020-03-25 16:35:41
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-25 16:44:39
 */
import React, { Component } from 'react';

const Computacion = () => {
    return (
        <React.Fragment>
            <li>Mouse</li>
            <li>Monitor</li>
            <li>Teclador</li>
            <li>Audifonos</li>
            <li>Impresoras</li>

        </React.Fragment> 
    );
}
 
const Ropa = () => {
    return (   <>
        <li>Playeras</li>
        <li>Camisas</li>
        <li>Vestidos</li>
        <li>Bolsos</li>
        <li>Zapatos</li>

    </>  );
}
 


class Fragmentos extends Component {
    state = {  }
    render() {
        return (<div>
            <Computacion>

            </Computacion>
            <Ropa/>
            </div>
        );
    }
}

export default Fragmentos;