/*
 * @Author: Your name
 * @Date:   2020-03-24 16:31:16
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-03-24 16:44:37
 */
import React, { Component } from 'react';

const Gato=(props)=>(
    <div>
        <h1>Gato <span role="img" aria-label="gato" >🐆</span></h1>
        <pre>
            {JSON.stringify(props,null,4)}
        </pre>
    </div>
)

class GATOS extends Component{
    state={
        name:"michu",
        fuerza:100,
        vidas:5
    }

    render(){
        const otrosDatos ={
            raza:'NIGA',
            peleas:1000    
        }

        return(
            <div>
                <Gato  // spread usa el object assign en la compilacion importa el orden
                name="gATOS"
                age='2 años'
                {...otrosDatos}
                {...this.state}>

                </Gato>
            </div>
        )
    }
}

export default GATOS