/*
 * @Author: Your name
 * @Date:   2020-04-08 22:58:36
 * @Last Modified by:   Your name
 * @Last Modified time: 2020-04-08 23:18:51
 */
'use strict';

export const UPDATE_NAME='UPDATE_NAME'

export const updateName=(name)=>{
    return {
        type:UPDATE_NAME,
        payload:{
            name
        }
    }
}